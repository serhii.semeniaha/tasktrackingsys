﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTOs.Project
{
  public class ProjectDto
  {
    [Required(ErrorMessage = "Property ID is required")]
    public Guid Id { get; set; }

    [
      Required(ErrorMessage = "Name is required, please fill in the form"),
      MaxLength(120, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string Name { get; set; }

    [
      Required(ErrorMessage = "Description is required, please fill in the form"),
      MaxLength(500, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string Description { get; set; }

    public ICollection<UserProjectDto> UserProjects { get; set; }
  }
}