﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTOs.Project
{
  public class UserProjectDto
  {
    [Required(ErrorMessage = "Project Id is required")]
    public Guid ProjectId { get; set; }

    [Required(ErrorMessage = "User Id is required")]
    public Guid UserId { get; set; }
  }
}