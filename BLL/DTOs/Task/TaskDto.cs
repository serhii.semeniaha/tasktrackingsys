﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTOs.Task
{
  public class TaskDto
  {
    [Required(ErrorMessage = "Task ID is required")]
    public Guid Id { get; set; }

    [
      Required(ErrorMessage = "Task name is required, please fill in the form"),
      MaxLength(120, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string Name { get; set; }

    [
      Required(ErrorMessage = "Task description is required, please fill in the form"),
      MaxLength(500, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string Description { get; set; }
    public string CreationDate { get; set; }
    public string LastUpdate { get; set; }
    public string Deadline { get; set; }
    public string TaskStatus { get; set; }
    [Required(ErrorMessage = "TaskStatus Id is required")]
    public int TaskStatusId { get; set; }
    public string TaskPriority { get; set; }
    [Required(ErrorMessage = "TaskPriority Id is required")]
    public int TaskPriorityId { get; set; }
    [Required(ErrorMessage = "Project Id is required")]
    public Guid ProjectId { get; set; }
    public Guid? UserId { get; set; }
  }
}