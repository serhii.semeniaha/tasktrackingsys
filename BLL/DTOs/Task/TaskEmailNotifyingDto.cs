﻿using BLL.DTOs.User;
using System;

namespace BLL.DTOs.Task
{
  public class TaskEmailNotifyingDto
  {
    public Guid Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string Deadline { get; set; }

    public UserDto User { get; set; }
  }
}
