﻿using System.ComponentModel.DataAnnotations;

namespace BLL.DTOs.User
{
  public class LoginDto
  {
    [Required(ErrorMessage = "Login is required, please fill in the form")]
    public string Login { get; set; }

    [Required(ErrorMessage = "Password is required, please fill in the form")]
    public string Password { get; set; }
  }
}