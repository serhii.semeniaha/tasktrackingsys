﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTOs.User
{
  public class RegistrationDto
  {
    [Required(ErrorMessage = "Property ID is required")]
    public Guid Id { get; set; }

    [
      Required(ErrorMessage = "FirstName is required, please fill in the form"),
      MaxLength(50, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string FirstName { get; set; }

    [
      Required(ErrorMessage = "Surname is required, please fill in the form"),
      MaxLength(50, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string Surname { get; set; }

    [StringLength(25, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 6)]
    public string Password { get; set; }

    [
      Required(ErrorMessage = "Email is required, please fill in the form"),
      EmailAddress,
      MaxLength(100, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string Email { get; set; }
    public string UserName { get; set; }
  }
}