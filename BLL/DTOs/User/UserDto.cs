﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTOs.User
{
  public class UserDto
  {
    public Guid Id { get; set; }

    [
      Required(ErrorMessage = "FirstName is required, please fill in the form"),
      MaxLength(50, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string FirstName { get; set; }

    [
      Required(ErrorMessage = "Surname is required, please fill in the form"),
      MaxLength(50, ErrorMessage = "{0} length must be less than or equal {1} letters")
    ]
    public string Surname { get; set; }

    public string Email { get; set; }
  }
}