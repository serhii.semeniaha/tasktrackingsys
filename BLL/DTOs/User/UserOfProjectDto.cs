﻿using System;

namespace BLL.DTOs.User
{
  public class UserOfProjectDto
  {
    public Guid Id { get; set; }
    public string FullName { get; set; }
  }
}