﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTOs.User
{
  public class UserSetRoleDto
  {
    [Required(ErrorMessage = "Property ID is required")]
    public Guid Id { get; set; }

    [Required(ErrorMessage = "FullName is required, please fill in the form")]
    public string FullName { get; set; }

    [Required(ErrorMessage = "Role is required, please fill in the form")]
    public string Role { get; set; }
  }
}