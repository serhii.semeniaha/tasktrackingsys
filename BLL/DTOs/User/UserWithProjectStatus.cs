﻿using System;

namespace BLL.DTOs.User
{
  public class UserWithProjectStatus
  {
    public Guid UserId { get; set; }
    public string FullName { get; set; }
    public bool IsWithProject { get; set; }
  }
}