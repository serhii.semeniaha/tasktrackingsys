﻿using System;
using System.Runtime.Serialization;

namespace BLL.Exceptions
{
  [Serializable]
  public class DbQueryNullException : Exception
  {
    public DbQueryNullException()
    { }

    public DbQueryNullException(string message) : base(message)
    { }

    public DbQueryNullException(string message, Exception inner) : base(message, inner)
    { }

    protected DbQueryNullException(SerializationInfo info, StreamingContext context) : base(info, context)
    { }
  }
}
