﻿using AutoMapper;
using BLL.Extensions.Settings;
using BLL.MapperProfiles;
using BLL.Services.Interfaces;
using BLL.Services.Realizations;
using DAL.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BLL.Extensions
{
  public static class BllDependeciesExtension
  {
    public static IServiceCollection AddBllDependecies(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddDalDependencies(configuration.GetConnectionString("MSSQLDBTaskTrackDBConnection"));
      var mapperConfig = new MapperConfiguration(mc =>
      {
        mc.AddProfile(new BllAutoMapperProfile());
      });
      services.AddSingleton(mapperConfig.CreateMapper());

      services.AddScoped<IAuthService, AuthService>();
      services.AddScoped<IUserService, UserService>();
      services.AddScoped<IProjectService, ProjectService>();
      services.AddScoped<ITaskService, TaskService>();
      services.AddScoped<IEmailService, EmailService>();
      services.AddScoped(typeof(ILoggerService<>), typeof(LoggerService<>));

      services.Configure<MailSettings>(configuration.GetSection("MailSettings"));
      services.Configure<JwtTokenSettings>(configuration.GetSection("JwtTokenSettings"));
      return services;
    }
  }
}
