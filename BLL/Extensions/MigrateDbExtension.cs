﻿using DAL.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BLL.Extensions
{
  public static class MigrateDbExtension
  {
    public static void MigrateDb(this IApplicationBuilder appBuilder)
    {
      using (var scope = appBuilder.ApplicationServices.GetService<IServiceScopeFactory>()?.CreateScope())
      {
        if (scope == null) return;
        var context = scope.ServiceProvider.GetRequiredService<TaskTrackingSysContext>();
        context.Database.Migrate();
      }
    }
  }
}
