﻿namespace BLL.Extensions.Settings
{
  public class JwtTokenSettings
  {
    public string Key { get; set; }
    public int ExpiryMinutes { get; set; }
  }
}