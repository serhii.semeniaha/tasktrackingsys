﻿using AutoMapper;
using BLL.DTOs.Project;
using BLL.DTOs.Task;
using BLL.DTOs.User;
using DAL.Entities;

namespace BLL.MapperProfiles
{
  public class BllAutoMapperProfile : Profile
  {
    public BllAutoMapperProfile()
    {

      CreateMap<Project, ProjectDto>().ReverseMap();
      CreateMap<UserProject, UserProjectDto>().ReverseMap();
      CreateMap<User, UserDto>().ReverseMap();

      CreateMap<User, UserOfProjectDto>()
        .ForMember(dest => dest.FullName, opt => opt.MapFrom(u => $"{u.Surname} {u.FirstName}"))
        .ReverseMap();

      CreateMap<User, RegistrationDto>().ReverseMap();

      CreateMap<Taskk, TaskDto>()
        .ForMember(dest => dest.TaskPriorityId, opt => opt.MapFrom(t => t.TaskPriority))
        .ForMember(dest => dest.TaskPriority, opt => opt.MapFrom(t => t.TaskPriority.ToString()))
        .ForMember(dest => dest.TaskStatusId, opt => opt.MapFrom(t => t.TaskStatus))
        .ForMember(dest => dest.TaskStatus, opt => opt.MapFrom(t => t.TaskStatus.ToString()))
        .ForMember(dest => dest.CreationDate, opt => opt.MapFrom(t => t.CreationDate.ToString("yyyy-MM-dd")))
        .ForMember(dest => dest.Deadline, opt => opt.MapFrom(source => source.Deadline.ToString("yyyy-MM-dd")))
        .ForMember(dest => dest.LastUpdate, opt => opt.MapFrom(source => source.LastUpdate.ToString("yyyy-MM-dd")))
        .ReverseMap();
    }
  }
}
