﻿using BLL.DTOs.User;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
  public interface IAuthService
  {
    Task<string> SignInAsync(LoginDto loginDto);
    Task<UserDto> SignUpAsync(RegistrationDto registrationDto);
    void LogOutAsync();
  }
}
