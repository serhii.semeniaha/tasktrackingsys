﻿using BLL.DTOs.Project;
using BLL.DTOs.Task;
using BLL.DTOs.User;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
  public interface IEmailService
  {
    Task SendEmailAboutNewProjectAsync(UserProjectDto userProjectDto);
    Task SendEmailAboutNewTaskAsync(TaskDto taskDto);
    Task SendEmailAboutChangedRoleAsync(UserSetRoleDto userRoleDto);
  }
}
