﻿using BLL.DTOs.Project;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
  public interface IProjectService
  {
    Task<IEnumerable<ProjectDto>> GetAllProjectsAsync();
    Task<IEnumerable<ProjectDto>> GetAllProjectsByUserIdAsync(Guid userId);
    Task<ProjectDto> GetByIdAsync(Guid id);
    Task<ProjectDto> CreateAsync(ProjectDto projectDto);
    Task UpdateAsync(ProjectDto projectDto);
    Task RemoveAsync(Guid projectId);
    Task AddUserToProjectAsync(UserProjectDto userProjDto);
    Task RemoveUserFromProjectAsync(UserProjectDto userProjDto);
  }
}
