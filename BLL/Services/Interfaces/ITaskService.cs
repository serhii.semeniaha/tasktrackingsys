﻿using BLL.DTOs.Task;
using System;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
  public interface ITaskService
  {
    Task<TasksOfProject> GetTasksByProjectIdAsync(Guid projectId, Guid userId);
    Task<TaskDto> GetByIdAsync(Guid id);
    Task<TaskDto> CreateAsync(TaskDto taskDto);
    Task UpdateAsync(TaskDto taskDto, Guid userId);
    Task RemoveAsync(Guid taskId);
  }
}
