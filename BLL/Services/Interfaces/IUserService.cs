﻿using BLL.DTOs.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
  public interface IUserService
  {
    Task<IEnumerable<UserSetRoleDto>> GetEmployeesAndManagersAsync(Guid adminId);
    Task<UserDto> GetByIdAsync(Guid id);
    Task SetUserRoleAsync(UserSetRoleDto userRoleDto);
    Task UpdateAsync(UserDto userDto);
    Task DeleteByIdAsync(Guid id);
    Task<IEnumerable<UserOfProjectDto>> GetUsersOfProjectAsync(Guid projectId);
    Task<IEnumerable<UserWithProjectStatus>> GetUsersWithProjectStatusAsync(Guid projectId);
  }
}
