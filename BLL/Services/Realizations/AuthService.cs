﻿using AutoMapper;
using BLL.DTOs.User;
using BLL.Exceptions;
using BLL.Extensions.Settings;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.Repo.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services.Realizations
{
  internal class AuthService : IAuthService
  {
    private readonly IUoW _uow;
    private readonly IMapper _mapper;
    private readonly JwtTokenSettings _jwtTokenSettings;
    private readonly SignInManager<User> _signInManager;

    public AuthService(IUoW uow, IMapper mapper, IOptions<JwtTokenSettings> jwtToken,
      SignInManager<User> signInManager)
    {
      _uow = uow;
      _mapper = mapper;
      _jwtTokenSettings = jwtToken.Value;
      _signInManager = signInManager;
    }

    public async Task<string> SignInAsync(LoginDto loginDto)
    {
      var user = await _uow.UserManager.FindByEmailAsync(loginDto.Login);

      if (user == null)
        throw new InvalidDataException("User with this login doesn't exist");
      if (!await _uow.UserManager.CheckPasswordAsync(user, loginDto.Password))
        throw new InvalidDataException("This password is incorrect");

      return await CreateJwtToken(user);
    }

    public async Task<UserDto> SignUpAsync(RegistrationDto regDto)
    {
      if (_uow.UserManager.Users.Any(u => u.Email == regDto.Email))
        throw new InvalidDataException("User with such an email already exists");

      var user = _mapper.Map<User>(regDto);

      var res = await _uow.UserManager.CreateAsync(user, regDto.Password);
      if (!res.Succeeded)
        throw new DbQueryNullException(res.Errors?.FirstOrDefault()?.Description);

      res = await _uow.UserManager.AddToRoleAsync(user, RoleType.EMPLOYEE);
      if (!res.Succeeded)
        throw new DbQueryNullException(res.Errors?.FirstOrDefault()?.Description);

      return _mapper.Map<UserDto>(user);
    }

    public async void LogOutAsync()
    {
      await _signInManager.SignOutAsync();
    }

    private async Task<string> CreateJwtToken(User user)
    {
      var roles = await _uow.UserManager.GetRolesAsync(user);
      var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtTokenSettings.Key));
      var utcNow = DateTime.UtcNow;

      var claims = new List<Claim>
      {
        new Claim("UserId", user.Id.ToString()),
        new Claim(JwtRegisteredClaimNames.Email, user.Email),
        new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
      };
      claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

      var claimsIdentity = new ClaimsIdentity(claims);
      var signingCred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

      var jwt = new JwtSecurityToken(
          signingCredentials: signingCred,
          claims: claimsIdentity.Claims,
          notBefore: utcNow,
          expires: utcNow.AddMinutes(_jwtTokenSettings.ExpiryMinutes)
      );
      var tokenHandler = new JwtSecurityTokenHandler();
      return tokenHandler.WriteToken(jwt);
    }
  }
}
