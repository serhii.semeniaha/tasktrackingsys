﻿using BLL.DTOs.Project;
using BLL.DTOs.Task;
using BLL.DTOs.User;
using BLL.Exceptions;
using BLL.Extensions.Settings;
using BLL.Models.Mail;
using BLL.Services.Interfaces;
using DAL.Repo.Interfaces;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MimeKit;
using System.Threading;
using System.Threading.Tasks;

namespace BLL.Services.Realizations
{
  internal class EmailService : IEmailService
  {
    private readonly IUoW _uow;
    private readonly MailSettings _mailSettings;

    public EmailService(IOptions<MailSettings> mailSettings, IUoW uow)
    {
      _uow = uow;
      _mailSettings = mailSettings.Value;
    }

    public async Task SendEmailAboutNewProjectAsync(UserProjectDto userProjectDto)
    {
      var project = await _uow.ProjectRepo.GetFirstOrDefaultAsync(p => p.Id == userProjectDto.ProjectId);

      if (project == null)
        throw new DbQueryNullException("This project doesn't exist");

      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == userProjectDto.UserId);

      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      var request = new MailRequest()
      {
        ToEmail = user.Email,
        Subject = "You have a new project",
        Body = $"Hi! You were added to new project - {project.Name}!"
      };

      await SendEmailAsync(request);
    }

    public async Task SendEmailAboutNewTaskAsync(TaskDto taskDto)
    {
      if (taskDto.UserId == null)
        throw new InvalidDataException("You didn't add user to task");

      var task = await _uow.TaskRepo.GetFirstOrDefaultAsync(t => t.Id == taskDto.Id);
      if (task == null)
        throw new DbQueryNullException("This task doesn't exist");

      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == taskDto.UserId);
      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      var request = new MailRequest()
      {
        ToEmail = user.Email,
        Subject = $"Hi {user.FirstName}, You have a new task",
        Body = $"{task.Description}"
      };

      await SendEmailAsync(request);
    }

    public async Task SendEmailAboutChangedRoleAsync(UserSetRoleDto userRoleDto)
    {
      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == userRoleDto.Id);

      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      var request = new MailRequest()
      {
        ToEmail = user.Email,
        Subject = $"Hi, {user.FirstName}! Your role was changed.",
        Body = $"Now, your role in 'Task tracning system' is {userRoleDto.Role}"
      };
      await SendEmailAsync(request);
    }

    private async Task SendEmailAsync(MailRequest mailRequest)
    {
      var mimeMess = new MimeMessage { Sender = new MailboxAddress(_mailSettings.DisplayName, _mailSettings.Mail) };

      mimeMess.Subject = mailRequest.Subject;
      mimeMess.Body = new BodyBuilder { HtmlBody = mailRequest.Body }.ToMessageBody();
      mimeMess.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
      ThreadPool.QueueUserWorkItem(async o =>
      {
        using (var smtp = new SmtpClient())
        {
          await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
          await smtp.AuthenticateAsync(_mailSettings.Mail, _mailSettings.Password);
          await smtp.SendAsync(mimeMess);
          await smtp.DisconnectAsync(true);
        }
      });
    }
  }
}
