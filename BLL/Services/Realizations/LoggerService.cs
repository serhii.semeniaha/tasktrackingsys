﻿using BLL.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace BLL.Services.Realizations
{
  public class LoggerService<T> : ILoggerService<T>
  {
    protected readonly ILogger<T> _logger;
    public LoggerService(ILogger<T> logger)
    {
      _logger = logger;
    }
    public void LogInformation(string msg)
    {
      _logger.Log(LogLevel.Information, $"{typeof(T)} - {msg}");
    }
    public void LogWarning(string msg)
    {
      _logger.Log(LogLevel.Warning, $"{typeof(T)} - {msg}");
    }
    public void LogTrace(string msg)
    {
      _logger.Log(LogLevel.Trace, $"{typeof(T)} - {msg}");
    }
    public void LogDebug(string msg)
    {
      _logger.Log(LogLevel.Debug, $"{typeof(T)} - {msg}");
    }
    public void LogError(string msg)
    {
      _logger.Log(LogLevel.Error, $"{typeof(T)} - {msg}");
    }
  }
}
