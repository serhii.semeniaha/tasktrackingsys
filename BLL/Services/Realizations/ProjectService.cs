﻿using AutoMapper;
using BLL.DTOs.Project;
using BLL.Exceptions;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.Repo.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskStatus = Shared.Enums.TaskStatus;

namespace BLL.Services.Realizations
{
  internal class ProjectService : IProjectService
  {
    private readonly IUoW _uow;
    private readonly IMapper _mapper;

    public ProjectService(IUoW uow, IMapper mapper)
    {
      _uow = uow;
      _mapper = mapper;
    }

    public async Task AddUserToProjectAsync(UserProjectDto userProjDto)
    {
      var project = await _uow.ProjectRepo.GetFirstOrDefaultWithIncludingAsync(p => p.Id == userProjDto.ProjectId, p => p.Include(p => p.UserProjects));

      if (project == null)
        throw new DbQueryNullException("This project doesn't exist");

      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == userProjDto.UserId);

      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      if (project.UserProjects.Any(up => up.UserId == userProjDto.UserId))
        throw new InvalidDataException("This user is already added");

      project.UserProjects.Add(_mapper.Map<UserProject>(userProjDto));

      _uow.ProjectRepo.Update(project);

      if (!await _uow.SaveAsync())
        throw new DbQueryNullException("An error occurred while saving changes");
    }

    public async Task<ProjectDto> CreateAsync(ProjectDto projectDto)
    {
      //if (projectDto.UserProjects.Count == 0 || projectDto.UserProjects == null)
      //  throw new InvalidDataException("User(s) is required, please add user(s) to project");

      //if (projectDto.UserProjects.Any(up => _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == up.UserId) == null))
      //  throw new InvalidDataException("User doesn't exist");

      var project = _mapper.Map<Project>(projectDto);

      await _uow.ProjectRepo.CreateAsync(project);
      if (!await _uow.SaveAsync())
        throw new DbQueryNullException("An error occurred while saving changes");

      return _mapper.Map<ProjectDto>(project);
    }

    public async Task<IEnumerable<ProjectDto>> GetAllProjectsAsync()
    {
      var projects = await _uow.ProjectRepo.GetAllAsync();

      return _mapper.Map<IEnumerable<ProjectDto>>(projects);
    }

    public async Task<IEnumerable<ProjectDto>> GetAllProjectsByUserIdAsync(Guid userId)
    {
      var projects = await _uow.ProjectRepo.GetAllAsync(pr => pr.UserProjects.Any(up => up.UserId == userId));

      return _mapper.Map<IEnumerable<ProjectDto>>(projects);
    }

    public async Task<ProjectDto> GetByIdAsync(Guid id)
    {
      var project = await _uow.ProjectRepo.GetFirstOrDefaultAsync(p => p.Id == id);

      if (project == null)
        throw new DbQueryNullException("This project doesn't exist");

      return _mapper.Map<ProjectDto>(project);
    }

    public async Task RemoveAsync(Guid projectId)
    {
      var project = await _uow.ProjectRepo.GetFirstOrDefaultAsync(p => p.Id == projectId);
      if (project == null)
        throw new DbQueryNullException("This project doesn't exist");

      _uow.ProjectRepo.Delete(project);
      if (!await _uow.SaveAsync())
        throw new DbQueryNullException("An error occurred while saving changes");
    }

    public async Task RemoveUserFromProjectAsync(UserProjectDto userProjDto)
    {
      var project = await _uow.ProjectRepo.GetFirstOrDefaultWithIncludingAsync(
        p => p.Id == userProjDto.ProjectId,
        p => p.Include(p => p.UserProjects));

      if (project == null)
        throw new DbQueryNullException("This project doesn't exist");

      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == userProjDto.UserId);

      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      var deletedUserProj = project.UserProjects.FirstOrDefault(up => up.UserId == userProjDto.UserId);
      if (deletedUserProj == null)
        throw new InvalidDataException("This user is already deleted");

      var userTasks = await _uow.TaskRepo.GetAllAsync(t =>
                              t.UserId == userProjDto.UserId
                              && t.ProjectId == userProjDto.ProjectId
                              && t.TaskStatus != TaskStatus.Completed);

      foreach (var task in userTasks)
      {
        task.TaskStatus = TaskStatus.Scheduled;
        task.LastUpdate = DateTime.Now;
        task.UserId = null;
        _uow.TaskRepo.Update(task);
      }

      project.UserProjects = project.UserProjects.Where(up => up.UserId != userProjDto.UserId).ToList();
      _uow.ProjectRepo.Update(project);

      if (!await _uow.SaveAsync())
        throw new DbQueryNullException("An error occurred while saving changes");
    }

    public async Task UpdateAsync(ProjectDto projectDto)
    {
      if (await _uow.ProjectRepo.GetFirstOrDefaultAsync(p => p.Id == projectDto.Id) == null)
        throw new DbQueryNullException("This project doesn't exist");

      var project = _mapper.Map<Project>(projectDto);

      _uow.ProjectRepo.Update(project);
      if (!await _uow.SaveAsync())
        throw new DbQueryNullException("An error occurred while saving changes");
    }
  }
}
