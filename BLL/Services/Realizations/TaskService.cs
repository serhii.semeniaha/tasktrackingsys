﻿using AutoMapper;
using BLL.DTOs.Task;
using BLL.Exceptions;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.Repo.Interfaces;
using Microsoft.EntityFrameworkCore;
using Shared.Enums;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services.Realizations
{
  internal class TaskService : ITaskService
  {
    private readonly IUoW _uow;
    private readonly IMapper _mapper;

    public TaskService(IUoW uow, IMapper mapper)
    {
      _uow = uow;
      _mapper = mapper;
    }

    public async Task<TaskDto> CreateAsync(TaskDto taskDto)
    {
      var project = await _uow.ProjectRepo.GetFirstOrDefaultAsync(p => p.Id == taskDto.ProjectId);

      if (project == null)
        throw new DbQueryNullException("This project doesn't exist");

      if (!Enum.IsDefined(typeof(Shared.Enums.TaskStatus), taskDto.TaskStatusId))
        throw new InvalidDataException("Incorrect TaskStatusId");

      if (!Enum.IsDefined(typeof(TaskPriority), taskDto.TaskPriorityId))
        throw new InvalidDataException("Incorrect TaskPriorityId");

      if (taskDto.UserId != null)
      {
        var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == taskDto.UserId);
        if (user == null)
          throw new DbQueryNullException("This user doesn't exist");
      }

      var task = _mapper.Map<Taskk>(taskDto);

      await _uow.TaskRepo.CreateAsync(task);

      if (!await _uow.SaveAsync())
        throw new DbQueryNullException("This task wasn't created");

      return taskDto;
    }

    public async Task<TasksOfProject> GetTasksByProjectIdAsync(Guid projectId, Guid userId)
    {
      var tasks = await _uow.TaskRepo.FindByCondition(t => t.ProjectId == projectId).ToListAsync();
      if (tasks == null)
        throw new DbQueryNullException("There aren't any tasks or the project doesn't exist");

      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == userId);
      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      if (await _uow.UserManager.IsInRoleAsync(user, RoleType.MANAGER))
        return new TasksOfProject()
        {
          Tasks = _mapper.Map<IEnumerable<TaskDto>>(tasks),
          PercentageCompletion = Math.Round(tasks.Count(t => t.TaskStatus == Shared.Enums.TaskStatus.Completed)
                                 / (double)tasks.Count(t => t.TaskStatus != Shared.Enums.TaskStatus.Cancelled), 4) * 100
        };


      return new TasksOfProject()
      {
        Tasks = _mapper.Map<IEnumerable<TaskDto>>(tasks.Where(t => t.UserId == userId)),
        PercentageCompletion = Math.Round(tasks.Count(t => t.TaskStatus == Shared.Enums.TaskStatus.Completed && t.UserId == userId)
                                 / (double)tasks.Count(t => t.TaskStatus != Shared.Enums.TaskStatus.Cancelled && t.UserId == userId), 4) * 100
      };
    }

    public async Task<TaskDto> GetByIdAsync(Guid id)
    {
      var task = await _uow.TaskRepo.GetFirstOrDefaultAsync(t => t.Id == id);
      if (task == null)
        throw new DbQueryNullException("This task doesn't exist");

      return _mapper.Map<TaskDto>(task);
    }

    public async Task RemoveAsync(Guid taskId)
    {
      var task = await _uow.TaskRepo.GetFirstOrDefaultAsync(t => t.Id == taskId);

      if (task == null)
        throw new DbQueryNullException("This task doesn't exist");

      _uow.TaskRepo.Delete(task);
      if (!await _uow.SaveAsync())
        throw new DbQueryNullException("This task wasn't removed");
    }

    public async Task UpdateAsync(TaskDto taskDto, Guid userId)
    {
      if (!Enum.IsDefined(typeof(Shared.Enums.TaskStatus), taskDto.TaskStatusId))
        throw new InvalidDataException("Incorrect TaskStatusId");

      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == userId);
      if (user != null)
      {
        if (await _uow.UserManager.IsInRoleAsync(user, RoleType.EMPLOYEE) && userId == taskDto.UserId)
        {
          var task = await _uow.TaskRepo.GetFirstOrDefaultAsync(t => t.Id == taskDto.Id);

          if (task == null)
            throw new DbQueryNullException("This task doesn't exist");

          task.TaskStatus = (Shared.Enums.TaskStatus)taskDto.TaskStatusId;
          _uow.TaskRepo.Update(task);
        }
        else if (await _uow.UserManager.IsInRoleAsync(user, RoleType.MANAGER))
        {
          var project = await _uow.ProjectRepo.GetFirstOrDefaultWithIncludingAsync(
            p => p.Id == taskDto.ProjectId,
            p => p.Include(p => p.UserProjects));

          if (project == null)
            throw new DbQueryNullException("This project doesn't exist");

          if (!Enum.IsDefined(typeof(TaskPriority), taskDto.TaskPriorityId))
            throw new InvalidDataException("Incorrect TaskPriorityId");

          if (taskDto.UserId != null)
          {
            var userOfProject = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == taskDto.UserId);
            if (userOfProject == null)
              throw new DbQueryNullException("Can't assign task to person that doesn't exists");

            if (project.UserProjects.All(up => up.UserId != taskDto.UserId))
              throw new DbQueryNullException("Can't assign task to person that doesn't exists in this project");
          }

          var task = await _uow.TaskRepo.GetFirstOrDefaultAsync(t => t.Id == taskDto.Id);
          if (task == null)
            throw new DbQueryNullException("This task doesn't exist");

          _uow.TaskRepo.Update(_mapper.Map<Taskk>(taskDto));

        }
      }

      if (!await _uow.SaveAsync())
        throw new DbQueryNullException("This task wasn't updated");
    }
  }
}
