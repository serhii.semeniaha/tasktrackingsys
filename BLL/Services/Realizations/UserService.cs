﻿using AutoMapper;
using BLL.DTOs.User;
using BLL.Exceptions;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.Repo.Interfaces;
using Microsoft.EntityFrameworkCore;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskStatus = Shared.Enums.TaskStatus;

namespace BLL.Services.Realizations
{
  internal class UserService : IUserService
  {
    private readonly IUoW _uow;
    private readonly IMapper _mapper;

    public UserService(IUoW uow, IMapper mapper)
    {
      _uow = uow;
      _mapper = mapper;
    }

    public async Task<UserDto> GetByIdAsync(Guid id)
    {
      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == id);

      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      return _mapper.Map<UserDto>(user);
    }

    public async Task<IEnumerable<UserSetRoleDto>> GetEmployeesAndManagersAsync(Guid adminId)
    {
      var users = await _uow.UserManager.Users.Where(u => u.Id != adminId).ToListAsync();
      var employeesAndManagers = new List<UserSetRoleDto>();

      foreach (var user in users)
      {
        var roles = await _uow.UserManager.GetRolesAsync(user);
        employeesAndManagers.Add(new UserSetRoleDto()
        {
          Id = user.Id,
          FullName = $"{user.Surname} {user.FirstName}",
          Role = roles.FirstOrDefault()
        });
      }
      return employeesAndManagers;
    }

    public async Task<IEnumerable<UserOfProjectDto>> GetUsersOfProjectAsync(Guid projectId)
    {
      var project = await _uow.ProjectRepo.GetFirstOrDefaultWithIncludingAsync(
        p => p.Id == projectId,
        p => p.Include(p => p.UserProjects).ThenInclude(up => up.User));

      if (project == null)
        throw new DbQueryNullException("Project with this id doesn't exist");

      return _mapper.Map<IEnumerable<UserOfProjectDto>>(project.UserProjects.Select(up => up.User));
    }

    public async Task<IEnumerable<UserWithProjectStatus>> GetUsersWithProjectStatusAsync(Guid projectId)
    {
      var project = await _uow.ProjectRepo.GetFirstOrDefaultAsync(p => p.Id == projectId);

      if (project == null)
        throw new DbQueryNullException("Project with this id doesn't exist");

      var usersWithoutProject = await _uow.UserManager.Users
          .Where(u => u.UserProjects.All(up => up.ProjectId != projectId)).ToListAsync();

      var usersWithProject = await _uow.UserManager.Users
          .Where(u => u.UserProjects.Any(up => up.ProjectId == projectId)).ToListAsync();

      return await CreateUserListWithProjectStatus(usersWithProject, usersWithoutProject);
    }

    public async Task SetUserRoleAsync(UserSetRoleDto userRoleDto)
    {
      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == userRoleDto.Id);

      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      if (!RoleType.ListOfRoles.Contains(userRoleDto.Role))
        throw new InvalidDataException("User role is invalid");

      if (await _uow.UserManager.IsInRoleAsync(user, userRoleDto.Role))
        throw new InvalidDataException("User is in this role already");

      var userRoles = await _uow.UserManager.GetRolesAsync(user);
      await _uow.UserManager.RemoveFromRolesAsync(user, userRoles);
      await _uow.UserManager.AddToRoleAsync(user, userRoleDto.Role);

      var result = await _uow.UserManager.UpdateAsync(user);

      if (!result.Succeeded)
        throw new InvalidDataException(result.Errors?.FirstOrDefault()?.Description);
    }

    public async Task UpdateAsync(UserDto userDto)
    {
      var user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Id == userDto.Id);

      if (user == null)
        throw new DbQueryNullException("This user doesn't exist");

      var result = await _uow.UserManager.UpdateAsync(_mapper.Map<User>(userDto));

      if (!result.Succeeded)
        throw new InvalidDataException(result.Errors?.FirstOrDefault()?.Description);
    }

    public async Task DeleteByIdAsync(Guid id)
    {
      var user = await _uow.UserManager.Users.Include(u => u.Tasks).FirstOrDefaultAsync(u => u.Id == id);

      if (user == null)
        throw new DbQueryNullException("User doesn't exist");

      foreach (var task in user.Tasks)
      {
        if (task.TaskStatus != TaskStatus.Completed && task.TaskStatus != TaskStatus.Cancelled)
          task.TaskStatus = TaskStatus.Scheduled;
      }

      await _uow.UserManager.DeleteAsync(user);
    }

    private async Task<IEnumerable<UserWithProjectStatus>> CreateUserListWithProjectStatus(IEnumerable<User> usersWithProject, IEnumerable<User> usersWithoutProject)
    {
      var userList = new List<UserWithProjectStatus>();

      foreach (var user in usersWithProject)
      {
        if (await _uow.UserManager.IsInRoleAsync(user, RoleType.EMPLOYEE))
          userList.Add(new UserWithProjectStatus()
          {
            UserId = user.Id,
            FullName = user.FirstName + " " + user.Surname,
            IsWithProject = true,
          });
      }

      foreach (var user in usersWithoutProject)
      {
        if (await _uow.UserManager.IsInRoleAsync(user, RoleType.EMPLOYEE))
          userList.Add(new UserWithProjectStatus()
          {
            UserId = user.Id,
            FullName = user.FirstName + " " + user.Surname,
            IsWithProject = false,
          });
      }

      return userList;
    }
  }
}
