﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configurations
{
  internal class TaskConfig : IEntityTypeConfiguration<Taskk>
  {
    public void Configure(EntityTypeBuilder<Taskk> builder)
    {
      builder.ToTable("Tasks");
      builder.Property(t => t.CreationDate)
        .HasDefaultValueSql("GETDATE()");

      builder.Property(t => t.LastUpdate)
        .HasDefaultValueSql("GETDATE()");

      builder.Property(t => t.Deadline)
        .IsRequired();

      builder.Property(t => t.TaskStatus)
        .HasConversion<int>()
        .IsRequired();

      builder.Property(t => t.TaskPriority)
        .HasConversion<int>()
        .IsRequired();

      builder.Property(t => t.Name)
        .HasMaxLength(120)
        .IsRequired();

      builder.Property(t => t.Description)
        .HasMaxLength(500)
        .IsRequired();

      builder.HasOne(t => t.User)
        .WithMany(u => u.Tasks)
        .OnDelete(DeleteBehavior.SetNull);

      builder.HasOne(t => t.Project)
        .WithMany(p => p.Tasks);
    }
  }
}
