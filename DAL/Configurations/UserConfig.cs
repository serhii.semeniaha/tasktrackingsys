﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configurations
{
  internal class UserConfig : IEntityTypeConfiguration<User>
  {
    public void Configure(EntityTypeBuilder<User> builder)
    {
      builder.Property(u => u.FirstName)
        .IsRequired()
        .HasMaxLength(50);

      builder.Property(u => u.Surname)
        .IsRequired()
        .HasMaxLength(50);

      builder.Property(u => u.Email)
        .IsRequired()
        .HasMaxLength(100);
    }
  }
}
