﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Shared.Enums;
using Shared.Models;
using System;
using System.Collections.Generic;

namespace DAL.Context
{
  public static class SeedStartData
  {
    public static void Seed(ModelBuilder builder)
    {

      var userRoles = new[]
      {
        new UserRole { Id = Guid.NewGuid(), Name = RoleType.ADMIN, NormalizedName = RoleType.ADMIN.ToUpper() },
        new UserRole { Id = Guid.NewGuid(), Name = RoleType.MANAGER, NormalizedName = RoleType.MANAGER.ToUpper() },
        new UserRole { Id = Guid.NewGuid(), Name = RoleType.EMPLOYEE, NormalizedName = RoleType.EMPLOYEE.ToUpper() }
      };

      builder.Entity<UserRole>().HasData(userRoles);

      var passwordHasher = new PasswordHasher<User>();
      var users = new[]
        {
          new User
          {
              Id = Guid.NewGuid(),
              UserName = "alex",
              NormalizedUserName = "alex".ToUpper(),
              FirstName = "Alex",
              Surname = "Shoroh",
              Email = "alex@gmail.com",
              NormalizedEmail = "alex@gmail.com".ToUpper(),
              EmailConfirmed = true,
              PasswordHash = passwordHasher.HashPassword(null, "11Qwerty"),
              SecurityStamp = Guid.NewGuid().ToString()
          },

          new User
          {
              Id = Guid.NewGuid(),
              UserName = "serg.seoweb",
              NormalizedUserName = "serg.seoweb".ToUpper(),
              FirstName = "Serg",
              Surname = "Crazy",
              Email = "serg.seoweb@gmail.com",
              NormalizedEmail = "serg.seoweb@gmail.com".ToUpper(),
              EmailConfirmed = true,
              PasswordHash = passwordHasher.HashPassword(null, "11Qwerty"),
              SecurityStamp = Guid.NewGuid().ToString()
          },

          new User
          {
              Id = Guid.NewGuid(),
              UserName = "ns22091",
              NormalizedUserName = "ns22091".ToUpper(),
              FirstName = "Nikita",
              Surname = "Sidorov",
              Email = "ns22091@gmail.com",
              NormalizedEmail = "ns22091@gmail.com".ToUpper(),
              EmailConfirmed = true,
              PasswordHash = passwordHasher.HashPassword(null, "11Qwerty"),
              SecurityStamp = Guid.NewGuid().ToString()
          },

          new User
          {
              Id = Guid.NewGuid(),
              UserName = "serg",
              NormalizedUserName = "serg".ToUpper(),
              FirstName = "Serg",
              Surname = "Simson",
              Email = "serg@gmail.com",
              NormalizedEmail = "serg@gmail.com".ToUpper(),
              EmailConfirmed = true,
              PasswordHash = passwordHasher.HashPassword(null, "11Qwerty"),
              SecurityStamp = Guid.NewGuid().ToString()
          }
        };
      builder.Entity<User>().HasData(users);

      builder.Entity<IdentityUserRole<Guid>>().HasData(
        new IdentityUserRole<Guid> { UserId = users[0].Id, RoleId = userRoles[2].Id },
        new IdentityUserRole<Guid> { UserId = users[1].Id, RoleId = userRoles[2].Id },
        new IdentityUserRole<Guid> { UserId = users[2].Id, RoleId = userRoles[1].Id },
        new IdentityUserRole<Guid> { UserId = users[3].Id, RoleId = userRoles[0].Id }
      );


      var projects = new[]
      {
        new Project { Id = Guid.NewGuid(), Name = "Project 1", Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", UserProjects = new List<UserProject>() },
        new Project { Id = Guid.NewGuid(), Name = "Project 2", Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", UserProjects = new List<UserProject>() },
        new Project { Id = Guid.NewGuid(), Name = "Project 3", Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", UserProjects = new List<UserProject>() }
      };

      builder.Entity<Project>().HasData(projects);

      var userProj = new[]
      {
        new UserProject { ProjectId = projects[0].Id, UserId = users[0].Id },
        new UserProject { ProjectId = projects[0].Id, UserId = users[1].Id },
        new UserProject { ProjectId = projects[2].Id, UserId = users[0].Id }
      };
      builder.Entity<UserProject>().HasData(userProj);

      builder.Entity<Taskk>().HasData(
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 1",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(4),
          TaskStatus = TaskStatus.Pending,
          TaskPriority = TaskPriority.Medium,
          ProjectId = projects[0].Id,
          UserId = users[0].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 2",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(3),
          TaskStatus = TaskStatus.In_Progress,
          TaskPriority = TaskPriority.Low,
          ProjectId = projects[0].Id,
          UserId = users[0].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 3",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(1),
          TaskStatus = TaskStatus.Completed,
          TaskPriority = TaskPriority.High,
          ProjectId = projects[0].Id,
          UserId = users[0].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 4",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(6),
          TaskStatus = TaskStatus.In_Progress,
          TaskPriority = TaskPriority.Medium,
          ProjectId = projects[0].Id,
          UserId = users[0].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 5",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(2),
          TaskStatus = TaskStatus.Scheduled,
          TaskPriority = TaskPriority.Low,
          ProjectId = projects[0].Id,
          UserId = users[1].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 6",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(4),
          TaskStatus = TaskStatus.Completed,
          TaskPriority = TaskPriority.High,
          ProjectId = projects[0].Id,
          UserId = users[1].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 7",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(3),
          TaskStatus = TaskStatus.Review,
          TaskPriority = TaskPriority.Medium,
          ProjectId = projects[0].Id,
          UserId = users[0].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 8",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(5),
          TaskStatus = TaskStatus.Scheduled,
          TaskPriority = TaskPriority.Critical,
          ProjectId = projects[0].Id,
          UserId = users[1].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 9",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(1),
          TaskStatus = TaskStatus.In_Progress,
          TaskPriority = TaskPriority.Critical,
          ProjectId = projects[0].Id,
          UserId = users[1].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 10",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(3),
          TaskStatus = TaskStatus.Pending,
          TaskPriority = TaskPriority.Low,
          ProjectId = projects[0].Id,
          UserId = users[0].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 11",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(6),
          TaskStatus = TaskStatus.Cancelled,
          TaskPriority = TaskPriority.Medium,
          ProjectId = projects[0].Id,
          UserId = users[1].Id
        },
        new Taskk
        {
          Id = Guid.NewGuid(),
          Name = "Task 12",
          Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
          Deadline = DateTime.Now.AddDays(4),
          TaskStatus = TaskStatus.Pending,
          TaskPriority = TaskPriority.High,
          ProjectId = projects[2].Id,
          UserId = users[0].Id
        }
      );
    }
  }
}