﻿using DAL.Configurations;
using DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace DAL.Context
{
  public class TaskTrackingSysContext : IdentityDbContext<User, UserRole, Guid>
  {
    public DbSet<Project> Projects { get; set; }
    public DbSet<Taskk> Tasks { get; set; }
    public DbSet<UserProject> UserProjects { get; set; }
    public TaskTrackingSysContext(DbContextOptions<TaskTrackingSysContext> options) : base(options)
    { }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
      builder.ApplyConfiguration(new UserConfig());
      builder.ApplyConfiguration(new ProjectConfig());
      builder.ApplyConfiguration(new TaskConfig());
      builder.ApplyConfiguration(new UserProjectConfig());
      SeedStartData.Seed(builder);

    }
  }
}
