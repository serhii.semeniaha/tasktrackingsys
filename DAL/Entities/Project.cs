﻿using System.Collections.Generic;

namespace DAL.Entities
{
  public class Project : BaseEntity
  {
    public string Name { get; set; }
    public string Description { get; set; }
    public bool IsPublished { get; set; }
    public bool IsCompleted { get; set; }
    public ICollection<UserProject> UserProjects { get; set; }
    public ICollection<Taskk> Tasks { get; set; }

  }
}
