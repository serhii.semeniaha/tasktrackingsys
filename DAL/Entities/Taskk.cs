﻿using Shared.Enums;
using System;

namespace DAL.Entities
{
  public class Taskk : BaseEntity
  {
    public string Name { get; set; }
    public string Description { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime LastUpdate { get; set; }
    public DateTime Deadline { get; set; }
    public TaskStatus TaskStatus { get; set; }
    public TaskPriority TaskPriority { get; set; }

    public Guid ProjectId { get; set; }
    public Project Project { get; set; }

    public Guid? UserId { get; set; }
    public User User { get; set; }
  }
}
