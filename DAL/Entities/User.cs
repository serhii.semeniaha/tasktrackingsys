﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace DAL.Entities
{
  public class User : IdentityUser<Guid>
  {
    public string FirstName { get; set; }
    public string Surname { get; set; }
    public ICollection<UserProject> UserProjects { get; set; }
    public ICollection<Taskk> Tasks { get; set; }

  }
}
