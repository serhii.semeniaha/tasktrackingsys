﻿using DAL.Context;
using DAL.Entities;
using DAL.Repo.Interfaces;
using DAL.Repo.Realizations;
using DAL.Repo.UoW;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DAL.Extensions
{
  public static class DalDependeciesExtension
  {
    public static IServiceCollection AddDalDependencies(this IServiceCollection services, string connectionString)
    {
      services.AddScoped<DbContext, TaskTrackingSysContext>();
      services.AddDbContext<TaskTrackingSysContext>(options => options.UseSqlServer(connectionString));
      services.AddIdentity<User, UserRole>(options => options.User.RequireUniqueEmail = true)
                .AddEntityFrameworkStores<TaskTrackingSysContext>();

      services.AddScoped<IUoW, UoW>();
      services.AddScoped<IProjectRepo, ProjectRepo>();
      services.AddScoped<ITaskRepo, TaskRepo>();

      return services;
    }
  }
}
