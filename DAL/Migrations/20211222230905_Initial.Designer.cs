﻿// <auto-generated />
using System;
using DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DAL.Migrations
{
    [DbContext(typeof(TaskTrackingSysContext))]
    [Migration("20211222230905_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.22")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DAL.Entities.Project", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(500)")
                        .HasMaxLength(500);

                    b.Property<bool>("IsCompleted")
                        .HasColumnType("bit");

                    b.Property<bool>("IsPublished")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(120)")
                        .HasMaxLength(120);

                    b.HasKey("Id");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            IsCompleted = false,
                            IsPublished = false,
                            Name = "Project 1"
                        },
                        new
                        {
                            Id = new Guid("222b8290-bee6-4090-8fd9-b0a5a82d1230"),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            IsCompleted = false,
                            IsPublished = false,
                            Name = "Project 2"
                        },
                        new
                        {
                            Id = new Guid("5fe98978-74cf-4236-a308-119aaa629d7e"),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            IsCompleted = false,
                            IsPublished = false,
                            Name = "Project 3"
                        });
                });

            modelBuilder.Entity("DAL.Entities.Taskk", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("CreationDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<DateTime>("Deadline")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(500)")
                        .HasMaxLength(500);

                    b.Property<DateTime>("LastUpdate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(120)")
                        .HasMaxLength(120);

                    b.Property<Guid>("ProjectId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("TaskPriority")
                        .HasColumnType("int");

                    b.Property<int>("TaskStatus")
                        .HasColumnType("int");

                    b.Property<Guid?>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("ProjectId");

                    b.HasIndex("UserId");

                    b.ToTable("Tasks");

                    b.HasData(
                        new
                        {
                            Id = new Guid("bc9a7269-c112-4a0c-ac13-52ec4912a698"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 27, 1, 9, 5, 600, DateTimeKind.Local).AddTicks(3457),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 1",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 2,
                            TaskStatus = 2,
                            UserId = new Guid("03734b49-685d-48aa-9c92-3801e72ee975")
                        },
                        new
                        {
                            Id = new Guid("4ecdec9f-3835-4e90-b413-781a0c238bd1"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3641),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 2",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 1,
                            TaskStatus = 3,
                            UserId = new Guid("03734b49-685d-48aa-9c92-3801e72ee975")
                        },
                        new
                        {
                            Id = new Guid("c4fbbe7d-1a59-4749-9ead-5fe5bd1fb52a"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 24, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3735),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 3",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 3,
                            TaskStatus = 5,
                            UserId = new Guid("03734b49-685d-48aa-9c92-3801e72ee975")
                        },
                        new
                        {
                            Id = new Guid("a05d7a85-5d24-416b-84c1-2685963811b0"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 29, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3744),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 4",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 2,
                            TaskStatus = 3,
                            UserId = new Guid("03734b49-685d-48aa-9c92-3801e72ee975")
                        },
                        new
                        {
                            Id = new Guid("893c6b70-5d7c-4427-9aea-a61a1a55a35c"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 25, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3756),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 5",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 1,
                            TaskStatus = 1,
                            UserId = new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39")
                        },
                        new
                        {
                            Id = new Guid("58e3b9d7-a752-43b2-ba39-09b47d6c6ed4"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 27, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3762),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 6",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 3,
                            TaskStatus = 5,
                            UserId = new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39")
                        },
                        new
                        {
                            Id = new Guid("aeb27007-88ce-4ad1-8af5-671928be14f2"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3767),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 7",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 2,
                            TaskStatus = 4,
                            UserId = new Guid("03734b49-685d-48aa-9c92-3801e72ee975")
                        },
                        new
                        {
                            Id = new Guid("d1620d8e-92ce-4e0e-b8da-4ed03056a672"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 28, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3771),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 8",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 4,
                            TaskStatus = 1,
                            UserId = new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39")
                        },
                        new
                        {
                            Id = new Guid("80d52e86-c152-4e01-90ba-91d53ec1a505"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 24, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3776),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 9",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 4,
                            TaskStatus = 3,
                            UserId = new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39")
                        },
                        new
                        {
                            Id = new Guid("8bc04521-b677-4eee-91a5-0eaa54d5b906"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3781),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 10",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 1,
                            TaskStatus = 2,
                            UserId = new Guid("03734b49-685d-48aa-9c92-3801e72ee975")
                        },
                        new
                        {
                            Id = new Guid("48d52cc7-9ec2-4981-b11b-5b47830306c0"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 29, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3785),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 11",
                            ProjectId = new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"),
                            TaskPriority = 2,
                            TaskStatus = 6,
                            UserId = new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39")
                        },
                        new
                        {
                            Id = new Guid("c72b6b78-d076-409e-af8d-0c25b1a5196c"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 27, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3790),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 12",
                            ProjectId = new Guid("5fe98978-74cf-4236-a308-119aaa629d7e"),
                            TaskPriority = 3,
                            TaskStatus = 2,
                            UserId = new Guid("03734b49-685d-48aa-9c92-3801e72ee975")
                        });
                });

            modelBuilder.Entity("DAL.Entities.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");

                    b.HasData(
                        new
                        {
                            Id = new Guid("03734b49-685d-48aa-9c92-3801e72ee975"),
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "20197939-6aa1-4e0d-91d2-62584d85b218",
                            Email = "alex@gmail.com",
                            EmailConfirmed = true,
                            FirstName = "Alex",
                            LockoutEnabled = false,
                            NormalizedEmail = "ALEX@GMAIL.COM",
                            NormalizedUserName = "ALEX",
                            PasswordHash = "AQAAAAEAACcQAAAAEEr1NSGDmX806tG2SLaBwpBNAaF6p7RtIBfAk4VeONELsWQpGwZ6FJiqbtS+p9OvBw==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "25c0b0f6-2e37-4fa4-91d1-cc0ceb265433",
                            Surname = "Shoroh",
                            TwoFactorEnabled = false,
                            UserName = "alex"
                        },
                        new
                        {
                            Id = new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39"),
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "893fc78b-b188-460a-a6fa-054973f9f15b",
                            Email = "kochka4real@gmail.com",
                            EmailConfirmed = true,
                            FirstName = "Danila",
                            LockoutEnabled = false,
                            NormalizedEmail = "KOCHKA4REAL@GMAIL.COM",
                            NormalizedUserName = "AOLAN13",
                            PasswordHash = "AQAAAAEAACcQAAAAEILiHwrOToHH5Rc0TyyaFslwB8LXs60/BI6GXHdRAqdP4SOOlO235bzh4VhJqJg1ZA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "0291476d-0cfa-4421-a6c6-d42d089b3cb1",
                            Surname = "Crazy",
                            TwoFactorEnabled = false,
                            UserName = "Aolan13"
                        },
                        new
                        {
                            Id = new Guid("b458ea21-4777-4dd3-88bf-d3d3084845cc"),
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "264d069b-33ce-4fa2-b387-8f3c859261a8",
                            Email = "ns18091@gmail.com",
                            EmailConfirmed = true,
                            FirstName = "Nikita",
                            LockoutEnabled = false,
                            NormalizedEmail = "NS18091@GMAIL.COM",
                            NormalizedUserName = "MXXNR1SE",
                            PasswordHash = "AQAAAAEAACcQAAAAEHCd285X0rS3RFC+iG23CRWeQLpHb1SgZpkqu8ZqIKNd0vc0vnf3D7+3nqNnEwHPAw==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "daf575a7-e728-4337-9953-a2204c333ccc",
                            Surname = "Sidorov",
                            TwoFactorEnabled = false,
                            UserName = "mxxnr1se"
                        },
                        new
                        {
                            Id = new Guid("e6fd24f9-8228-4d3f-8bc7-21f0bd20107f"),
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "e47b7ab1-df2e-4ace-992f-51f01449ca6b",
                            Email = "serg@gmail.com",
                            EmailConfirmed = true,
                            FirstName = "Serg",
                            LockoutEnabled = false,
                            NormalizedEmail = "SERG@GMAIL.COM",
                            NormalizedUserName = "SERG",
                            PasswordHash = "AQAAAAEAACcQAAAAEDl4IatfOavdBADkOPQT27CTgPfcJDFgD+0NW1PNnAieN+KptcSXANgY2R+sPJK6hA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "469bd40b-cf1a-4f0a-9b9b-15f1445af518",
                            Surname = "Simson",
                            TwoFactorEnabled = false,
                            UserName = "serg"
                        });
                });

            modelBuilder.Entity("DAL.Entities.UserProject", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("ProjectId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("UserId", "ProjectId");

                    b.HasIndex("ProjectId");

                    b.ToTable("UserProjects");
                });

            modelBuilder.Entity("DAL.Entities.UserRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");

                    b.HasData(
                        new
                        {
                            Id = new Guid("2cbce6bb-7398-4e52-8038-153d3ad688b1"),
                            ConcurrencyStamp = "7f4f8d6a-4935-4817-ba62-a8dc3ef8e743",
                            Name = "Admin",
                            NormalizedName = "ADMIN"
                        },
                        new
                        {
                            Id = new Guid("d976c631-7d67-4a7a-aba8-8e6bb47ad559"),
                            ConcurrencyStamp = "59d103df-79aa-421d-8865-cbd6428de729",
                            Name = "Manager",
                            NormalizedName = "MANAGER"
                        },
                        new
                        {
                            Id = new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af"),
                            ConcurrencyStamp = "c624125a-fc5e-47c1-b6ca-98130aee484a",
                            Name = "Employee",
                            NormalizedName = "EMPLOYEE"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("RoleId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<System.Guid>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("RoleId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");

                    b.HasData(
                        new
                        {
                            UserId = new Guid("03734b49-685d-48aa-9c92-3801e72ee975"),
                            RoleId = new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af")
                        },
                        new
                        {
                            UserId = new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39"),
                            RoleId = new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af")
                        },
                        new
                        {
                            UserId = new Guid("b458ea21-4777-4dd3-88bf-d3d3084845cc"),
                            RoleId = new Guid("d976c631-7d67-4a7a-aba8-8e6bb47ad559")
                        },
                        new
                        {
                            UserId = new Guid("e6fd24f9-8228-4d3f-8bc7-21f0bd20107f"),
                            RoleId = new Guid("2cbce6bb-7398-4e52-8038-153d3ad688b1")
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("DAL.Entities.Taskk", b =>
                {
                    b.HasOne("DAL.Entities.Project", "Project")
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", "User")
                        .WithMany("Tasks")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("DAL.Entities.UserProject", b =>
                {
                    b.HasOne("DAL.Entities.Project", "Project")
                        .WithMany("UserProjects")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", "User")
                        .WithMany("UserProjects")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.UserRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.UserRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
