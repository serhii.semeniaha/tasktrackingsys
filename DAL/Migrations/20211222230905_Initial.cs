﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    Surname = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 120, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: false),
                    IsPublished = table.Column<bool>(nullable: false),
                    IsCompleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 120, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    LastUpdate = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    Deadline = table.Column<DateTime>(nullable: false),
                    TaskStatus = table.Column<int>(nullable: false),
                    TaskPriority = table.Column<int>(nullable: false),
                    ProjectId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "UserProjects",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    ProjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProjects", x => new { x.UserId, x.ProjectId });
                    table.ForeignKey(
                        name: "FK_UserProjects_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserProjects_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("2cbce6bb-7398-4e52-8038-153d3ad688b1"), "7f4f8d6a-4935-4817-ba62-a8dc3ef8e743", "Admin", "ADMIN" },
                    { new Guid("d976c631-7d67-4a7a-aba8-8e6bb47ad559"), "59d103df-79aa-421d-8865-cbd6428de729", "Manager", "MANAGER" },
                    { new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af"), "c624125a-fc5e-47c1-b6ca-98130aee484a", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("03734b49-685d-48aa-9c92-3801e72ee975"), 0, "20197939-6aa1-4e0d-91d2-62584d85b218", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEEr1NSGDmX806tG2SLaBwpBNAaF6p7RtIBfAk4VeONELsWQpGwZ6FJiqbtS+p9OvBw==", null, false, "25c0b0f6-2e37-4fa4-91d1-cc0ceb265433", "Shoroh", false, "alex" },
                    { new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39"), 0, "893fc78b-b188-460a-a6fa-054973f9f15b", "kochka4real@gmail.com", true, "Danila", false, null, "KOCHKA4REAL@GMAIL.COM", "AOLAN13", "AQAAAAEAACcQAAAAEILiHwrOToHH5Rc0TyyaFslwB8LXs60/BI6GXHdRAqdP4SOOlO235bzh4VhJqJg1ZA==", null, false, "0291476d-0cfa-4421-a6c6-d42d089b3cb1", "Crazy", false, "Aolan13" },
                    { new Guid("b458ea21-4777-4dd3-88bf-d3d3084845cc"), 0, "264d069b-33ce-4fa2-b387-8f3c859261a8", "ns18091@gmail.com", true, "Nikita", false, null, "NS18091@GMAIL.COM", "MXXNR1SE", "AQAAAAEAACcQAAAAEHCd285X0rS3RFC+iG23CRWeQLpHb1SgZpkqu8ZqIKNd0vc0vnf3D7+3nqNnEwHPAw==", null, false, "daf575a7-e728-4337-9953-a2204c333ccc", "Sidorov", false, "mxxnr1se" },
                    { new Guid("e6fd24f9-8228-4d3f-8bc7-21f0bd20107f"), 0, "e47b7ab1-df2e-4ace-992f-51f01449ca6b", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAEDl4IatfOavdBADkOPQT27CTgPfcJDFgD+0NW1PNnAieN+KptcSXANgY2R+sPJK6hA==", null, false, "469bd40b-cf1a-4f0a-9b9b-15f1445af518", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("222b8290-bee6-4090-8fd9-b0a5a82d1230"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("5fe98978-74cf-4236-a308-119aaa629d7e"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("e6fd24f9-8228-4d3f-8bc7-21f0bd20107f"), new Guid("2cbce6bb-7398-4e52-8038-153d3ad688b1") },
                    { new Guid("b458ea21-4777-4dd3-88bf-d3d3084845cc"), new Guid("d976c631-7d67-4a7a-aba8-8e6bb47ad559") },
                    { new Guid("03734b49-685d-48aa-9c92-3801e72ee975"), new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af") },
                    { new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39"), new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("bc9a7269-c112-4a0c-ac13-52ec4912a698"), new DateTime(2021, 12, 27, 1, 9, 5, 600, DateTimeKind.Local).AddTicks(3457), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 2, 2, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("4ecdec9f-3835-4e90-b413-781a0c238bd1"), new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3641), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 1, 3, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("c4fbbe7d-1a59-4749-9ead-5fe5bd1fb52a"), new DateTime(2021, 12, 24, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3735), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 3, 5, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("a05d7a85-5d24-416b-84c1-2685963811b0"), new DateTime(2021, 12, 29, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3744), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 2, 3, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("aeb27007-88ce-4ad1-8af5-671928be14f2"), new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3767), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 2, 4, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("8bc04521-b677-4eee-91a5-0eaa54d5b906"), new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3781), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 1, 2, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("c72b6b78-d076-409e-af8d-0c25b1a5196c"), new DateTime(2021, 12, 27, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3790), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("5fe98978-74cf-4236-a308-119aaa629d7e"), 3, 2, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("893c6b70-5d7c-4427-9aea-a61a1a55a35c"), new DateTime(2021, 12, 25, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3756), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 1, 1, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") },
                    { new Guid("58e3b9d7-a752-43b2-ba39-09b47d6c6ed4"), new DateTime(2021, 12, 27, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3762), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 3, 5, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") },
                    { new Guid("d1620d8e-92ce-4e0e-b8da-4ed03056a672"), new DateTime(2021, 12, 28, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3771), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 4, 1, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") },
                    { new Guid("80d52e86-c152-4e01-90ba-91d53ec1a505"), new DateTime(2021, 12, 24, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3776), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 4, 3, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") },
                    { new Guid("48d52cc7-9ec2-4981-b11b-5b47830306c0"), new DateTime(2021, 12, 29, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3785), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 2, 6, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_UserId",
                table: "Tasks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProjects_ProjectId",
                table: "UserProjects",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "UserProjects");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
