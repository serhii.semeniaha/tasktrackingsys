﻿// <auto-generated />
using System;
using DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DAL.Migrations
{
    [DbContext(typeof(TaskTrackingSysContext))]
    [Migration("20211226003138_Changed_UserConfig")]
    partial class Changed_UserConfig
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.22")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DAL.Entities.Project", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(500)")
                        .HasMaxLength(500);

                    b.Property<bool>("IsCompleted")
                        .HasColumnType("bit");

                    b.Property<bool>("IsPublished")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(120)")
                        .HasMaxLength(120);

                    b.HasKey("Id");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            IsCompleted = false,
                            IsPublished = false,
                            Name = "Project 1"
                        },
                        new
                        {
                            Id = new Guid("51734933-b783-4304-b104-cd3c484427ec"),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            IsCompleted = false,
                            IsPublished = false,
                            Name = "Project 2"
                        },
                        new
                        {
                            Id = new Guid("e6be6114-9f30-454a-b15e-1986a537a7bf"),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            IsCompleted = false,
                            IsPublished = false,
                            Name = "Project 3"
                        });
                });

            modelBuilder.Entity("DAL.Entities.Taskk", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("CreationDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<DateTime>("Deadline")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(500)")
                        .HasMaxLength(500);

                    b.Property<DateTime>("LastUpdate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(120)")
                        .HasMaxLength(120);

                    b.Property<Guid>("ProjectId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("TaskPriority")
                        .HasColumnType("int");

                    b.Property<int>("TaskStatus")
                        .HasColumnType("int");

                    b.Property<Guid?>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("ProjectId");

                    b.HasIndex("UserId");

                    b.ToTable("Tasks");

                    b.HasData(
                        new
                        {
                            Id = new Guid("61d8acf6-22a2-4ed4-bdce-64452c48a4da"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 30, 2, 31, 38, 577, DateTimeKind.Local).AddTicks(6191),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 1",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 2,
                            TaskStatus = 2,
                            UserId = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6")
                        },
                        new
                        {
                            Id = new Guid("39372867-68fc-4ad7-82ac-44dbe3bba87f"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8280),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 2",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 1,
                            TaskStatus = 3,
                            UserId = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6")
                        },
                        new
                        {
                            Id = new Guid("62100842-d871-4ac2-bde2-79a0498a07d9"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 27, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8350),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 3",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 3,
                            TaskStatus = 5,
                            UserId = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6")
                        },
                        new
                        {
                            Id = new Guid("af119374-2a30-4edf-86bc-4f578c284f9d"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2022, 1, 1, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8357),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 4",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 2,
                            TaskStatus = 3,
                            UserId = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6")
                        },
                        new
                        {
                            Id = new Guid("e363e18a-22d1-490b-8755-6afc2ad5ce03"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 28, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8369),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 5",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 1,
                            TaskStatus = 1,
                            UserId = new Guid("6b392dee-232b-411d-8bef-c1206830da3a")
                        },
                        new
                        {
                            Id = new Guid("478c7016-fe67-4017-8f76-758533c68d2e"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 30, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8374),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 6",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 3,
                            TaskStatus = 5,
                            UserId = new Guid("6b392dee-232b-411d-8bef-c1206830da3a")
                        },
                        new
                        {
                            Id = new Guid("2b6f2fe0-6631-4bed-87dd-01388a46133f"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8380),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 7",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 2,
                            TaskStatus = 4,
                            UserId = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6")
                        },
                        new
                        {
                            Id = new Guid("d1cc7b7f-0c27-4947-a6cd-232e7c59180f"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 31, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8386),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 8",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 4,
                            TaskStatus = 1,
                            UserId = new Guid("6b392dee-232b-411d-8bef-c1206830da3a")
                        },
                        new
                        {
                            Id = new Guid("99d14d9f-1b72-448b-9671-d35c38d39040"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 27, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8391),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 9",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 4,
                            TaskStatus = 3,
                            UserId = new Guid("6b392dee-232b-411d-8bef-c1206830da3a")
                        },
                        new
                        {
                            Id = new Guid("c9b82f50-9676-4d3e-bfca-46b96dd74843"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8395),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 10",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 1,
                            TaskStatus = 2,
                            UserId = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6")
                        },
                        new
                        {
                            Id = new Guid("7515d473-cd8c-4e7e-b78b-fc61a632cdd5"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2022, 1, 1, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8399),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 11",
                            ProjectId = new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"),
                            TaskPriority = 2,
                            TaskStatus = 6,
                            UserId = new Guid("6b392dee-232b-411d-8bef-c1206830da3a")
                        },
                        new
                        {
                            Id = new Guid("195ffce3-c0ad-4ec2-961f-088771501c21"),
                            CreationDate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 30, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8403),
                            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            LastUpdate = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Task 12",
                            ProjectId = new Guid("e6be6114-9f30-454a-b15e-1986a537a7bf"),
                            TaskPriority = 3,
                            TaskStatus = 2,
                            UserId = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6")
                        });
                });

            modelBuilder.Entity("DAL.Entities.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");

                    b.HasData(
                        new
                        {
                            Id = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"),
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "0bc5b2a8-560e-413a-a07e-e20ed1ed9d38",
                            Email = "alex@gmail.com",
                            EmailConfirmed = true,
                            FirstName = "Alex",
                            LockoutEnabled = false,
                            NormalizedEmail = "ALEX@GMAIL.COM",
                            NormalizedUserName = "ALEX",
                            PasswordHash = "AQAAAAEAACcQAAAAEI0xaAyvB06h6fKDzI+3ShQWzdifgwwqjl1KfbApUJ0zC4OqYKVQGbGVZ0pHX9WFkQ==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "3f2fba40-cbc2-410d-8d4b-dd4e2e4dc328",
                            Surname = "Shoroh",
                            TwoFactorEnabled = false,
                            UserName = "alex"
                        },
                        new
                        {
                            Id = new Guid("6b392dee-232b-411d-8bef-c1206830da3a"),
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "55754e7a-0ec8-4725-bf8f-1c39c66b11e8",
                            Email = "kochka4real@gmail.com",
                            EmailConfirmed = true,
                            FirstName = "Danila",
                            LockoutEnabled = false,
                            NormalizedEmail = "KOCHKA4REAL@GMAIL.COM",
                            NormalizedUserName = "AOLAN13",
                            PasswordHash = "AQAAAAEAACcQAAAAELtHacVVwywh9MGBGL40ArKviOUKDioLoAUdPnOWXHMnqyWtrOvejjXHueWpeEvkGg==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "ca856a02-ca4b-470b-9a73-6e0a5acaa443",
                            Surname = "Crazy",
                            TwoFactorEnabled = false,
                            UserName = "Aolan13"
                        },
                        new
                        {
                            Id = new Guid("877bc514-63fc-4b55-9f81-f7144d403772"),
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "8c65f3da-2a68-4419-9c41-c3ecc05a03b1",
                            Email = "ns18091@gmail.com",
                            EmailConfirmed = true,
                            FirstName = "Nikita",
                            LockoutEnabled = false,
                            NormalizedEmail = "NS18091@GMAIL.COM",
                            NormalizedUserName = "MXXNR1SE",
                            PasswordHash = "AQAAAAEAACcQAAAAEL2aCpN6IW39YHV3VhxQTMQkKVpFxSuEuCUC3YJa18ZnB29Bd9iexyJliHm1eRlqeQ==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "0e8c0538-2f1a-440a-9921-cb2974dd2500",
                            Surname = "Sidorov",
                            TwoFactorEnabled = false,
                            UserName = "mxxnr1se"
                        },
                        new
                        {
                            Id = new Guid("b7a23a6a-d254-496a-9461-2e3200786135"),
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "746e1e76-1333-4126-91a6-6825853e43a2",
                            Email = "serg@gmail.com",
                            EmailConfirmed = true,
                            FirstName = "Serg",
                            LockoutEnabled = false,
                            NormalizedEmail = "SERG@GMAIL.COM",
                            NormalizedUserName = "SERG",
                            PasswordHash = "AQAAAAEAACcQAAAAED4g+n1WODUc/DWU1iPz1u/LHClAaLteSzNf0OhKSXfWARd84xFyeLbjNAKBH5vjMA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "dac1aace-bde7-4555-b463-8556e91c2ca8",
                            Surname = "Simson",
                            TwoFactorEnabled = false,
                            UserName = "serg"
                        });
                });

            modelBuilder.Entity("DAL.Entities.UserProject", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("ProjectId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("UserId", "ProjectId");

                    b.HasIndex("ProjectId");

                    b.ToTable("UserProjects");
                });

            modelBuilder.Entity("DAL.Entities.UserRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");

                    b.HasData(
                        new
                        {
                            Id = new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f"),
                            ConcurrencyStamp = "acc6c28c-781c-4483-82bb-8f3cacd67015",
                            Name = "Admin",
                            NormalizedName = "ADMIN"
                        },
                        new
                        {
                            Id = new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d"),
                            ConcurrencyStamp = "50402f92-130d-4d1d-bf33-071da7c88690",
                            Name = "Manager",
                            NormalizedName = "MANAGER"
                        },
                        new
                        {
                            Id = new Guid("774005b7-444d-45f3-8785-fda79391f03b"),
                            ConcurrencyStamp = "12ac1240-a0b3-4a6f-a830-f2a9cc094408",
                            Name = "Employee",
                            NormalizedName = "EMPLOYEE"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("RoleId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<System.Guid>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("RoleId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");

                    b.HasData(
                        new
                        {
                            UserId = new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"),
                            RoleId = new Guid("774005b7-444d-45f3-8785-fda79391f03b")
                        },
                        new
                        {
                            UserId = new Guid("6b392dee-232b-411d-8bef-c1206830da3a"),
                            RoleId = new Guid("774005b7-444d-45f3-8785-fda79391f03b")
                        },
                        new
                        {
                            UserId = new Guid("877bc514-63fc-4b55-9f81-f7144d403772"),
                            RoleId = new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d")
                        },
                        new
                        {
                            UserId = new Guid("b7a23a6a-d254-496a-9461-2e3200786135"),
                            RoleId = new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f")
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("DAL.Entities.Taskk", b =>
                {
                    b.HasOne("DAL.Entities.Project", "Project")
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", "User")
                        .WithMany("Tasks")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("DAL.Entities.UserProject", b =>
                {
                    b.HasOne("DAL.Entities.Project", "Project")
                        .WithMany("UserProjects")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", "User")
                        .WithMany("UserProjects")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.UserRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.UserRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<System.Guid>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
