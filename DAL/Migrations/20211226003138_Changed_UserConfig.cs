﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Changed_UserConfig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("03734b49-685d-48aa-9c92-3801e72ee975"), new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39"), new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("b458ea21-4777-4dd3-88bf-d3d3084845cc"), new Guid("d976c631-7d67-4a7a-aba8-8e6bb47ad559") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("e6fd24f9-8228-4d3f-8bc7-21f0bd20107f"), new Guid("2cbce6bb-7398-4e52-8038-153d3ad688b1") });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("222b8290-bee6-4090-8fd9-b0a5a82d1230"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("48d52cc7-9ec2-4981-b11b-5b47830306c0"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("4ecdec9f-3835-4e90-b413-781a0c238bd1"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("58e3b9d7-a752-43b2-ba39-09b47d6c6ed4"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("80d52e86-c152-4e01-90ba-91d53ec1a505"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("893c6b70-5d7c-4427-9aea-a61a1a55a35c"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("8bc04521-b677-4eee-91a5-0eaa54d5b906"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("a05d7a85-5d24-416b-84c1-2685963811b0"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("aeb27007-88ce-4ad1-8af5-671928be14f2"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("bc9a7269-c112-4a0c-ac13-52ec4912a698"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("c4fbbe7d-1a59-4749-9ead-5fe5bd1fb52a"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("c72b6b78-d076-409e-af8d-0c25b1a5196c"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("d1620d8e-92ce-4e0e-b8da-4ed03056a672"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cbce6bb-7398-4e52-8038-153d3ad688b1"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("d976c631-7d67-4a7a-aba8-8e6bb47ad559"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("03734b49-685d-48aa-9c92-3801e72ee975"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("b458ea21-4777-4dd3-88bf-d3d3084845cc"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("e6fd24f9-8228-4d3f-8bc7-21f0bd20107f"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("5fe98978-74cf-4236-a308-119aaa629d7e"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"));

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "AspNetUsers",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(256)",
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "AspNetUsers",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(256)",
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f"), "acc6c28c-781c-4483-82bb-8f3cacd67015", "Admin", "ADMIN" },
                    { new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d"), "50402f92-130d-4d1d-bf33-071da7c88690", "Manager", "MANAGER" },
                    { new Guid("774005b7-444d-45f3-8785-fda79391f03b"), "12ac1240-a0b3-4a6f-a830-f2a9cc094408", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"), 0, "0bc5b2a8-560e-413a-a07e-e20ed1ed9d38", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEI0xaAyvB06h6fKDzI+3ShQWzdifgwwqjl1KfbApUJ0zC4OqYKVQGbGVZ0pHX9WFkQ==", null, false, "3f2fba40-cbc2-410d-8d4b-dd4e2e4dc328", "Shoroh", false, "alex" },
                    { new Guid("6b392dee-232b-411d-8bef-c1206830da3a"), 0, "55754e7a-0ec8-4725-bf8f-1c39c66b11e8", "kochka4real@gmail.com", true, "Danila", false, null, "KOCHKA4REAL@GMAIL.COM", "AOLAN13", "AQAAAAEAACcQAAAAELtHacVVwywh9MGBGL40ArKviOUKDioLoAUdPnOWXHMnqyWtrOvejjXHueWpeEvkGg==", null, false, "ca856a02-ca4b-470b-9a73-6e0a5acaa443", "Crazy", false, "Aolan13" },
                    { new Guid("877bc514-63fc-4b55-9f81-f7144d403772"), 0, "8c65f3da-2a68-4419-9c41-c3ecc05a03b1", "ns18091@gmail.com", true, "Nikita", false, null, "NS18091@GMAIL.COM", "MXXNR1SE", "AQAAAAEAACcQAAAAEL2aCpN6IW39YHV3VhxQTMQkKVpFxSuEuCUC3YJa18ZnB29Bd9iexyJliHm1eRlqeQ==", null, false, "0e8c0538-2f1a-440a-9921-cb2974dd2500", "Sidorov", false, "mxxnr1se" },
                    { new Guid("b7a23a6a-d254-496a-9461-2e3200786135"), 0, "746e1e76-1333-4126-91a6-6825853e43a2", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAED4g+n1WODUc/DWU1iPz1u/LHClAaLteSzNf0OhKSXfWARd84xFyeLbjNAKBH5vjMA==", null, false, "dac1aace-bde7-4555-b463-8556e91c2ca8", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("51734933-b783-4304-b104-cd3c484427ec"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("e6be6114-9f30-454a-b15e-1986a537a7bf"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("b7a23a6a-d254-496a-9461-2e3200786135"), new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f") },
                    { new Guid("877bc514-63fc-4b55-9f81-f7144d403772"), new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d") },
                    { new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"), new Guid("774005b7-444d-45f3-8785-fda79391f03b") },
                    { new Guid("6b392dee-232b-411d-8bef-c1206830da3a"), new Guid("774005b7-444d-45f3-8785-fda79391f03b") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("61d8acf6-22a2-4ed4-bdce-64452c48a4da"), new DateTime(2021, 12, 30, 2, 31, 38, 577, DateTimeKind.Local).AddTicks(6191), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 2, 2, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("39372867-68fc-4ad7-82ac-44dbe3bba87f"), new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8280), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 1, 3, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("62100842-d871-4ac2-bde2-79a0498a07d9"), new DateTime(2021, 12, 27, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8350), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 3, 5, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("af119374-2a30-4edf-86bc-4f578c284f9d"), new DateTime(2022, 1, 1, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8357), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 2, 3, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("2b6f2fe0-6631-4bed-87dd-01388a46133f"), new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8380), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 2, 4, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("c9b82f50-9676-4d3e-bfca-46b96dd74843"), new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8395), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 1, 2, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("195ffce3-c0ad-4ec2-961f-088771501c21"), new DateTime(2021, 12, 30, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8403), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("e6be6114-9f30-454a-b15e-1986a537a7bf"), 3, 2, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("e363e18a-22d1-490b-8755-6afc2ad5ce03"), new DateTime(2021, 12, 28, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8369), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 1, 1, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") },
                    { new Guid("478c7016-fe67-4017-8f76-758533c68d2e"), new DateTime(2021, 12, 30, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8374), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 3, 5, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") },
                    { new Guid("d1cc7b7f-0c27-4947-a6cd-232e7c59180f"), new DateTime(2021, 12, 31, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8386), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 4, 1, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") },
                    { new Guid("99d14d9f-1b72-448b-9671-d35c38d39040"), new DateTime(2021, 12, 27, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8391), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 4, 3, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") },
                    { new Guid("7515d473-cd8c-4e7e-b78b-fc61a632cdd5"), new DateTime(2022, 1, 1, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8399), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 2, 6, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"), new Guid("774005b7-444d-45f3-8785-fda79391f03b") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("6b392dee-232b-411d-8bef-c1206830da3a"), new Guid("774005b7-444d-45f3-8785-fda79391f03b") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("877bc514-63fc-4b55-9f81-f7144d403772"), new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("b7a23a6a-d254-496a-9461-2e3200786135"), new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f") });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("51734933-b783-4304-b104-cd3c484427ec"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("195ffce3-c0ad-4ec2-961f-088771501c21"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("2b6f2fe0-6631-4bed-87dd-01388a46133f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("39372867-68fc-4ad7-82ac-44dbe3bba87f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("478c7016-fe67-4017-8f76-758533c68d2e"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("61d8acf6-22a2-4ed4-bdce-64452c48a4da"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("62100842-d871-4ac2-bde2-79a0498a07d9"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("7515d473-cd8c-4e7e-b78b-fc61a632cdd5"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("99d14d9f-1b72-448b-9671-d35c38d39040"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("af119374-2a30-4edf-86bc-4f578c284f9d"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("c9b82f50-9676-4d3e-bfca-46b96dd74843"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("d1cc7b7f-0c27-4947-a6cd-232e7c59180f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("e363e18a-22d1-490b-8755-6afc2ad5ce03"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("774005b7-444d-45f3-8785-fda79391f03b"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("6b392dee-232b-411d-8bef-c1206830da3a"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("877bc514-63fc-4b55-9f81-f7144d403772"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("b7a23a6a-d254-496a-9461-2e3200786135"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("e6be6114-9f30-454a-b15e-1986a537a7bf"));

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "AspNetUsers",
                type: "nvarchar(256)",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "AspNetUsers",
                type: "nvarchar(256)",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("2cbce6bb-7398-4e52-8038-153d3ad688b1"), "7f4f8d6a-4935-4817-ba62-a8dc3ef8e743", "Admin", "ADMIN" },
                    { new Guid("d976c631-7d67-4a7a-aba8-8e6bb47ad559"), "59d103df-79aa-421d-8865-cbd6428de729", "Manager", "MANAGER" },
                    { new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af"), "c624125a-fc5e-47c1-b6ca-98130aee484a", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("03734b49-685d-48aa-9c92-3801e72ee975"), 0, "20197939-6aa1-4e0d-91d2-62584d85b218", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEEr1NSGDmX806tG2SLaBwpBNAaF6p7RtIBfAk4VeONELsWQpGwZ6FJiqbtS+p9OvBw==", null, false, "25c0b0f6-2e37-4fa4-91d1-cc0ceb265433", "Shoroh", false, "alex" },
                    { new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39"), 0, "893fc78b-b188-460a-a6fa-054973f9f15b", "kochka4real@gmail.com", true, "Danila", false, null, "KOCHKA4REAL@GMAIL.COM", "AOLAN13", "AQAAAAEAACcQAAAAEILiHwrOToHH5Rc0TyyaFslwB8LXs60/BI6GXHdRAqdP4SOOlO235bzh4VhJqJg1ZA==", null, false, "0291476d-0cfa-4421-a6c6-d42d089b3cb1", "Crazy", false, "Aolan13" },
                    { new Guid("b458ea21-4777-4dd3-88bf-d3d3084845cc"), 0, "264d069b-33ce-4fa2-b387-8f3c859261a8", "ns18091@gmail.com", true, "Nikita", false, null, "NS18091@GMAIL.COM", "MXXNR1SE", "AQAAAAEAACcQAAAAEHCd285X0rS3RFC+iG23CRWeQLpHb1SgZpkqu8ZqIKNd0vc0vnf3D7+3nqNnEwHPAw==", null, false, "daf575a7-e728-4337-9953-a2204c333ccc", "Sidorov", false, "mxxnr1se" },
                    { new Guid("e6fd24f9-8228-4d3f-8bc7-21f0bd20107f"), 0, "e47b7ab1-df2e-4ace-992f-51f01449ca6b", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAEDl4IatfOavdBADkOPQT27CTgPfcJDFgD+0NW1PNnAieN+KptcSXANgY2R+sPJK6hA==", null, false, "469bd40b-cf1a-4f0a-9b9b-15f1445af518", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("222b8290-bee6-4090-8fd9-b0a5a82d1230"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("5fe98978-74cf-4236-a308-119aaa629d7e"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("e6fd24f9-8228-4d3f-8bc7-21f0bd20107f"), new Guid("2cbce6bb-7398-4e52-8038-153d3ad688b1") },
                    { new Guid("b458ea21-4777-4dd3-88bf-d3d3084845cc"), new Guid("d976c631-7d67-4a7a-aba8-8e6bb47ad559") },
                    { new Guid("03734b49-685d-48aa-9c92-3801e72ee975"), new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af") },
                    { new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39"), new Guid("6db9fc34-af9c-4fea-a75e-c422091cb2af") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("bc9a7269-c112-4a0c-ac13-52ec4912a698"), new DateTime(2021, 12, 27, 1, 9, 5, 600, DateTimeKind.Local).AddTicks(3457), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 2, 2, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("4ecdec9f-3835-4e90-b413-781a0c238bd1"), new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3641), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 1, 3, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("c4fbbe7d-1a59-4749-9ead-5fe5bd1fb52a"), new DateTime(2021, 12, 24, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3735), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 3, 5, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("a05d7a85-5d24-416b-84c1-2685963811b0"), new DateTime(2021, 12, 29, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3744), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 2, 3, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("aeb27007-88ce-4ad1-8af5-671928be14f2"), new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3767), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 2, 4, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("8bc04521-b677-4eee-91a5-0eaa54d5b906"), new DateTime(2021, 12, 26, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3781), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 1, 2, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("c72b6b78-d076-409e-af8d-0c25b1a5196c"), new DateTime(2021, 12, 27, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3790), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("5fe98978-74cf-4236-a308-119aaa629d7e"), 3, 2, new Guid("03734b49-685d-48aa-9c92-3801e72ee975") },
                    { new Guid("893c6b70-5d7c-4427-9aea-a61a1a55a35c"), new DateTime(2021, 12, 25, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3756), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 1, 1, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") },
                    { new Guid("58e3b9d7-a752-43b2-ba39-09b47d6c6ed4"), new DateTime(2021, 12, 27, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3762), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 3, 5, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") },
                    { new Guid("d1620d8e-92ce-4e0e-b8da-4ed03056a672"), new DateTime(2021, 12, 28, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3771), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 4, 1, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") },
                    { new Guid("80d52e86-c152-4e01-90ba-91d53ec1a505"), new DateTime(2021, 12, 24, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3776), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 4, 3, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") },
                    { new Guid("48d52cc7-9ec2-4981-b11b-5b47830306c0"), new DateTime(2021, 12, 29, 1, 9, 5, 602, DateTimeKind.Local).AddTicks(3785), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("bb7a0e48-719c-412b-8709-45a931fdcb1c"), 2, 6, new Guid("4e3f2f81-c3b8-43a0-af5e-9facebc8cd39") }
                });
        }
    }
}
