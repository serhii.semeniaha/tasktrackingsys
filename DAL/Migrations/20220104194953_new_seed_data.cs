﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class new_seed_data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"), new Guid("774005b7-444d-45f3-8785-fda79391f03b") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("6b392dee-232b-411d-8bef-c1206830da3a"), new Guid("774005b7-444d-45f3-8785-fda79391f03b") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("877bc514-63fc-4b55-9f81-f7144d403772"), new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("b7a23a6a-d254-496a-9461-2e3200786135"), new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f") });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("51734933-b783-4304-b104-cd3c484427ec"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("195ffce3-c0ad-4ec2-961f-088771501c21"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("2b6f2fe0-6631-4bed-87dd-01388a46133f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("39372867-68fc-4ad7-82ac-44dbe3bba87f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("478c7016-fe67-4017-8f76-758533c68d2e"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("61d8acf6-22a2-4ed4-bdce-64452c48a4da"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("62100842-d871-4ac2-bde2-79a0498a07d9"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("7515d473-cd8c-4e7e-b78b-fc61a632cdd5"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("99d14d9f-1b72-448b-9671-d35c38d39040"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("af119374-2a30-4edf-86bc-4f578c284f9d"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("c9b82f50-9676-4d3e-bfca-46b96dd74843"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("d1cc7b7f-0c27-4947-a6cd-232e7c59180f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("e363e18a-22d1-490b-8755-6afc2ad5ce03"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("774005b7-444d-45f3-8785-fda79391f03b"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("6b392dee-232b-411d-8bef-c1206830da3a"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("877bc514-63fc-4b55-9f81-f7144d403772"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("b7a23a6a-d254-496a-9461-2e3200786135"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("e6be6114-9f30-454a-b15e-1986a537a7bf"));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("82a907f0-31be-454f-974d-8f6ba827ff84"), "9a137ba5-a1dc-4f94-9aff-4530072472cd", "Admin", "ADMIN" },
                    { new Guid("0dd79ff4-eb8d-4c2e-8ea9-f61272df1d67"), "567e5747-b760-4e66-9650-255be764dd98", "Manager", "MANAGER" },
                    { new Guid("6149e83b-d7d9-4842-ba59-08927f63094e"), "b1e24036-86a6-4b5a-8357-8e69706a1153", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), 0, "6b3d1b85-bd5d-484e-a542-44207a7660af", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEEx1KemNeFroTpmOScDdNdLSXD7QlThJLkZvcsceCiccfR9cGzcT++FDX22inQlDuQ==", null, false, "b6030cf1-5e56-40dc-b92c-43e34781b716", "Shoroh", false, "alex" },
                    { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), 0, "89bb3284-0e26-4476-9fef-1d34d9d79ba7", "nocka4real@gmail.com", true, "Danila", false, null, "NOCKA4REAL@GMAIL.COM", "NOCKA4REAL", "AQAAAAEAACcQAAAAEH7pYA81w+QboV2cnwFsDQlVRvx4mRUcnG1KnIz3WiYSTyoyGY/2erAnAbG+uKwoZw==", null, false, "3ae7ffb5-de2c-42c4-83c7-406bac81f88a", "Crazy", false, "nocka4real" },
                    { new Guid("ca484fd1-6cfb-488b-b6dd-51d185c5f0e7"), 0, "e2019942-548e-4491-8969-f4fa35ee6a10", "ns22091@gmail.com", true, "Nikita", false, null, "NS22091@GMAIL.COM", "NS22091", "AQAAAAEAACcQAAAAEPVY97hg1JMWAvEnpbTRqwJARiNgqBtbwhR6enAvCqpNLDNqyab88d9TfvJsoIGxPg==", null, false, "a1ae9676-4a54-4af5-8f7e-0a0134abd486", "Sidorov", false, "ns22091" },
                    { new Guid("70bc1287-f913-4135-8069-22d359955aef"), 0, "312e1681-ef51-4587-a63c-0e51f491582c", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAEAU+mGyY+NN2f1Bx7ZR3A4aJAqZciyxVLPpXL7i9e4UqbpszqSKlFSZ6mG7zhPRgxA==", null, false, "4c244f58-5120-4cf3-8854-d3de4e652c75", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("ea150e13-68b2-4576-b6c3-73c66531aa9b"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), new Guid("6149e83b-d7d9-4842-ba59-08927f63094e") },
                    { new Guid("ca484fd1-6cfb-488b-b6dd-51d185c5f0e7"), new Guid("0dd79ff4-eb8d-4c2e-8ea9-f61272df1d67") },
                    { new Guid("70bc1287-f913-4135-8069-22d359955aef"), new Guid("82a907f0-31be-454f-974d-8f6ba827ff84") },
                    { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("6149e83b-d7d9-4842-ba59-08927f63094e") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("6d6569d8-e3ea-4240-9e32-e9b64b559056"), new DateTime(2022, 1, 10, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5884), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 2, 6, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("f1d0a29b-d875-4f7d-81a9-f0f3c9640a09"), new DateTime(2022, 1, 5, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5876), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 4, 3, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("797e9e28-94d2-4e82-9086-e3befda6bdcc"), new DateTime(2022, 1, 9, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5872), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 4, 1, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("6824baa9-b9e1-4445-a51b-9a5ba97aa4a4"), new DateTime(2022, 1, 8, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5863), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 3, 5, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("3cccfa6c-b004-4fbb-8c0a-4185ae46d5cb"), new DateTime(2022, 1, 6, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5857), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 1, 1, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("33b74936-2eda-4ad4-afff-0519aebdf7b1"), new DateTime(2022, 1, 8, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5889), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033"), 3, 2, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("480fce6a-2047-4a81-9c0d-7dcb59ce537f"), new DateTime(2022, 1, 7, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5880), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 1, 2, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("3f2e2084-7ce5-4c2d-9fb0-2cf637e8a2f5"), new DateTime(2022, 1, 7, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5867), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 2, 4, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("78fd5b95-066d-4039-83f0-1b4d09e6f53f"), new DateTime(2022, 1, 10, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5845), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 2, 3, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("04f58ab3-97cb-4bc2-9a40-966a463e1838"), new DateTime(2022, 1, 5, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5837), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 3, 5, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("cc4dfff3-658d-44e8-93d1-3c44919622ce"), new DateTime(2022, 1, 7, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5748), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 1, 3, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("1d520c6c-2c5d-4f1f-b189-0f005a4b80cb"), new DateTime(2022, 1, 8, 21, 49, 52, 878, DateTimeKind.Local).AddTicks(6732), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 2, 2, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") }
                });

            migrationBuilder.InsertData(
                table: "UserProjects",
                columns: new[] { "UserId", "ProjectId" },
                values: new object[,]
                {
                    { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729") },
                    { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729") },
                    { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("70bc1287-f913-4135-8069-22d359955aef"), new Guid("82a907f0-31be-454f-974d-8f6ba827ff84") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("6149e83b-d7d9-4842-ba59-08927f63094e") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("ca484fd1-6cfb-488b-b6dd-51d185c5f0e7"), new Guid("0dd79ff4-eb8d-4c2e-8ea9-f61272df1d67") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), new Guid("6149e83b-d7d9-4842-ba59-08927f63094e") });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("ea150e13-68b2-4576-b6c3-73c66531aa9b"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("04f58ab3-97cb-4bc2-9a40-966a463e1838"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("1d520c6c-2c5d-4f1f-b189-0f005a4b80cb"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("33b74936-2eda-4ad4-afff-0519aebdf7b1"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("3cccfa6c-b004-4fbb-8c0a-4185ae46d5cb"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("3f2e2084-7ce5-4c2d-9fb0-2cf637e8a2f5"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("480fce6a-2047-4a81-9c0d-7dcb59ce537f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("6824baa9-b9e1-4445-a51b-9a5ba97aa4a4"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("6d6569d8-e3ea-4240-9e32-e9b64b559056"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("78fd5b95-066d-4039-83f0-1b4d09e6f53f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("797e9e28-94d2-4e82-9086-e3befda6bdcc"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("cc4dfff3-658d-44e8-93d1-3c44919622ce"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("f1d0a29b-d875-4f7d-81a9-f0f3c9640a09"));

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729") });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("0dd79ff4-eb8d-4c2e-8ea9-f61272df1d67"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6149e83b-d7d9-4842-ba59-08927f63094e"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("82a907f0-31be-454f-974d-8f6ba827ff84"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("70bc1287-f913-4135-8069-22d359955aef"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("ca484fd1-6cfb-488b-b6dd-51d185c5f0e7"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f"), "acc6c28c-781c-4483-82bb-8f3cacd67015", "Admin", "ADMIN" },
                    { new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d"), "50402f92-130d-4d1d-bf33-071da7c88690", "Manager", "MANAGER" },
                    { new Guid("774005b7-444d-45f3-8785-fda79391f03b"), "12ac1240-a0b3-4a6f-a830-f2a9cc094408", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"), 0, "0bc5b2a8-560e-413a-a07e-e20ed1ed9d38", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEI0xaAyvB06h6fKDzI+3ShQWzdifgwwqjl1KfbApUJ0zC4OqYKVQGbGVZ0pHX9WFkQ==", null, false, "3f2fba40-cbc2-410d-8d4b-dd4e2e4dc328", "Shoroh", false, "alex" },
                    { new Guid("6b392dee-232b-411d-8bef-c1206830da3a"), 0, "55754e7a-0ec8-4725-bf8f-1c39c66b11e8", "kochka4real@gmail.com", true, "Danila", false, null, "KOCHKA4REAL@GMAIL.COM", "AOLAN13", "AQAAAAEAACcQAAAAELtHacVVwywh9MGBGL40ArKviOUKDioLoAUdPnOWXHMnqyWtrOvejjXHueWpeEvkGg==", null, false, "ca856a02-ca4b-470b-9a73-6e0a5acaa443", "Crazy", false, "Aolan13" },
                    { new Guid("877bc514-63fc-4b55-9f81-f7144d403772"), 0, "8c65f3da-2a68-4419-9c41-c3ecc05a03b1", "ns18091@gmail.com", true, "Nikita", false, null, "NS18091@GMAIL.COM", "MXXNR1SE", "AQAAAAEAACcQAAAAEL2aCpN6IW39YHV3VhxQTMQkKVpFxSuEuCUC3YJa18ZnB29Bd9iexyJliHm1eRlqeQ==", null, false, "0e8c0538-2f1a-440a-9921-cb2974dd2500", "Sidorov", false, "mxxnr1se" },
                    { new Guid("b7a23a6a-d254-496a-9461-2e3200786135"), 0, "746e1e76-1333-4126-91a6-6825853e43a2", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAED4g+n1WODUc/DWU1iPz1u/LHClAaLteSzNf0OhKSXfWARd84xFyeLbjNAKBH5vjMA==", null, false, "dac1aace-bde7-4555-b463-8556e91c2ca8", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("51734933-b783-4304-b104-cd3c484427ec"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("e6be6114-9f30-454a-b15e-1986a537a7bf"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("b7a23a6a-d254-496a-9461-2e3200786135"), new Guid("0f520d1b-225e-47c2-9e30-9081bda8b65f") },
                    { new Guid("877bc514-63fc-4b55-9f81-f7144d403772"), new Guid("dba315ba-2491-4684-ba3c-cd6ac369f27d") },
                    { new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6"), new Guid("774005b7-444d-45f3-8785-fda79391f03b") },
                    { new Guid("6b392dee-232b-411d-8bef-c1206830da3a"), new Guid("774005b7-444d-45f3-8785-fda79391f03b") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("61d8acf6-22a2-4ed4-bdce-64452c48a4da"), new DateTime(2021, 12, 30, 2, 31, 38, 577, DateTimeKind.Local).AddTicks(6191), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 2, 2, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("39372867-68fc-4ad7-82ac-44dbe3bba87f"), new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8280), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 1, 3, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("62100842-d871-4ac2-bde2-79a0498a07d9"), new DateTime(2021, 12, 27, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8350), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 3, 5, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("af119374-2a30-4edf-86bc-4f578c284f9d"), new DateTime(2022, 1, 1, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8357), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 2, 3, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("2b6f2fe0-6631-4bed-87dd-01388a46133f"), new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8380), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 2, 4, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("c9b82f50-9676-4d3e-bfca-46b96dd74843"), new DateTime(2021, 12, 29, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8395), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 1, 2, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("195ffce3-c0ad-4ec2-961f-088771501c21"), new DateTime(2021, 12, 30, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8403), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("e6be6114-9f30-454a-b15e-1986a537a7bf"), 3, 2, new Guid("483a27ca-e1d7-4ab9-8ca6-30eee4b672d6") },
                    { new Guid("e363e18a-22d1-490b-8755-6afc2ad5ce03"), new DateTime(2021, 12, 28, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8369), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 1, 1, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") },
                    { new Guid("478c7016-fe67-4017-8f76-758533c68d2e"), new DateTime(2021, 12, 30, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8374), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 3, 5, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") },
                    { new Guid("d1cc7b7f-0c27-4947-a6cd-232e7c59180f"), new DateTime(2021, 12, 31, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8386), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 4, 1, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") },
                    { new Guid("99d14d9f-1b72-448b-9671-d35c38d39040"), new DateTime(2021, 12, 27, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8391), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 4, 3, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") },
                    { new Guid("7515d473-cd8c-4e7e-b78b-fc61a632cdd5"), new DateTime(2022, 1, 1, 2, 31, 38, 579, DateTimeKind.Local).AddTicks(8399), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("ca07ee6e-fdbc-4330-9c3d-a830feab5ed3"), 2, 6, new Guid("6b392dee-232b-411d-8bef-c1206830da3a") }
                });
        }
    }
}
