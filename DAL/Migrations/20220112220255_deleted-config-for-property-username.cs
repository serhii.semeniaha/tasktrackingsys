﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class deletedconfigforpropertyusername : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("70bc1287-f913-4135-8069-22d359955aef"), new Guid("82a907f0-31be-454f-974d-8f6ba827ff84") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("6149e83b-d7d9-4842-ba59-08927f63094e") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("ca484fd1-6cfb-488b-b6dd-51d185c5f0e7"), new Guid("0dd79ff4-eb8d-4c2e-8ea9-f61272df1d67") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), new Guid("6149e83b-d7d9-4842-ba59-08927f63094e") });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("ea150e13-68b2-4576-b6c3-73c66531aa9b"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("04f58ab3-97cb-4bc2-9a40-966a463e1838"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("1d520c6c-2c5d-4f1f-b189-0f005a4b80cb"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("33b74936-2eda-4ad4-afff-0519aebdf7b1"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("3cccfa6c-b004-4fbb-8c0a-4185ae46d5cb"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("3f2e2084-7ce5-4c2d-9fb0-2cf637e8a2f5"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("480fce6a-2047-4a81-9c0d-7dcb59ce537f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("6824baa9-b9e1-4445-a51b-9a5ba97aa4a4"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("6d6569d8-e3ea-4240-9e32-e9b64b559056"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("78fd5b95-066d-4039-83f0-1b4d09e6f53f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("797e9e28-94d2-4e82-9086-e3befda6bdcc"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("cc4dfff3-658d-44e8-93d1-3c44919622ce"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("f1d0a29b-d875-4f7d-81a9-f0f3c9640a09"));

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729") });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("0dd79ff4-eb8d-4c2e-8ea9-f61272df1d67"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6149e83b-d7d9-4842-ba59-08927f63094e"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("82a907f0-31be-454f-974d-8f6ba827ff84"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("70bc1287-f913-4135-8069-22d359955aef"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("ca484fd1-6cfb-488b-b6dd-51d185c5f0e7"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"));

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "AspNetUsers",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("0806c7f8-3e4d-484f-b931-5bb3c232a23d"), "c6dd210a-3c44-4c0b-bc7d-ea41471592ed", "Admin", "ADMIN" },
                    { new Guid("f86a5963-0661-4d9c-aef1-e112dce86502"), "1e8fd8da-f466-4f9b-8243-f524d00fbaf2", "Manager", "MANAGER" },
                    { new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b"), "40fca424-aceb-4a39-8704-4c662dcc3262", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), 0, "d74d829b-fae4-4f9f-9d71-10f958f89b3f", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEAe1omowPWWibaTl0ixaKo8cYxwVu6nuqwyslvV77MrNfO0HQTUid3fNjG6Sx2ElXg==", null, false, "c609d238-dbe7-47e4-934e-5515930a4c44", "Shoroh", false, "alex" },
                    { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), 0, "9e617fa8-59c5-4737-ac8c-886081e722c6", "serg.seoweb@gmail.com", true, "Serg", false, null, "SERG.SEOWEB@GMAIL.COM", "SERG.SEOWEB", "AQAAAAEAACcQAAAAEMbY6JooJRenAZ8peLb4RpeiggZAF/S9tvwapSAT+0+7BqIYo64kWOqvqghHoztZRQ==", null, false, "56477b0b-70c8-4b26-ad7e-c0f6f1737e43", "Crazy", false, "serg.seoweb" },
                    { new Guid("94e180fe-dc12-4d9a-bfc2-f562fc6d7e4a"), 0, "cd0ce4f6-cb94-466e-8511-1a86652128d2", "ns22091@gmail.com", true, "Nikita", false, null, "NS22091@GMAIL.COM", "NS22091", "AQAAAAEAACcQAAAAEMP6NXF1kwxtGRD+CR+nHnuH/Jr4E46bR+zptnyoUoBx7m3+0q18PdvIOCgaGdjknA==", null, false, "0642dd98-5d80-4fec-b425-9b8c2b0aec93", "Sidorov", false, "ns22091" },
                    { new Guid("d1e9fb8e-9a25-41d1-844d-e14b4b1f6e41"), 0, "a536e479-b144-400e-8897-830fab014943", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAEAGpyAWYGsm6g8dWgpwcM3EpHZ92wlHbs1VgZG8fRDlOlOfTAwcZlmH4OcKjqOafyQ==", null, false, "8455e99d-1f88-4168-8782-b86470f79a17", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("6420c7df-f1f0-4e10-ad1b-8220834e44de"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b") },
                    { new Guid("94e180fe-dc12-4d9a-bfc2-f562fc6d7e4a"), new Guid("f86a5963-0661-4d9c-aef1-e112dce86502") },
                    { new Guid("d1e9fb8e-9a25-41d1-844d-e14b4b1f6e41"), new Guid("0806c7f8-3e4d-484f-b931-5bb3c232a23d") },
                    { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("d52662da-8429-46be-bcc8-141e875f8e96"), new DateTime(2022, 1, 19, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8912), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 2, 6, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("6481913d-1633-4778-8efe-249ac6f75679"), new DateTime(2022, 1, 14, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8903), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 4, 3, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("1f53c13c-05ca-4ea8-b5ec-addf4eb24e9c"), new DateTime(2022, 1, 18, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8899), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 4, 1, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("476aa937-03e6-4a39-908d-4f526bb9054f"), new DateTime(2022, 1, 17, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8890), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 3, 5, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("f885a95a-b573-4478-b7bf-c2c6e7cc7a71"), new DateTime(2022, 1, 15, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8884), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 1, 1, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("83abb046-b780-4700-9898-f44d5f4cac95"), new DateTime(2022, 1, 17, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8916), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105"), 3, 2, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("e4787505-2f47-4f3e-9900-d3c99249e505"), new DateTime(2022, 1, 16, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8907), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 1, 2, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("49120adc-d7d3-489e-813a-3321133876d6"), new DateTime(2022, 1, 16, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8894), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 2, 4, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("90ed8e01-3005-4395-87b3-9ece3ebd6be9"), new DateTime(2022, 1, 19, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8869), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 2, 3, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("dfbc2419-488d-4f64-aeb5-83bc2bf3ad50"), new DateTime(2022, 1, 14, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8861), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 3, 5, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("f01c4fac-09f8-4752-914a-91975b96779e"), new DateTime(2022, 1, 16, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8766), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 1, 3, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("731ebe18-53a5-4ea8-8e8c-f421791ae98e"), new DateTime(2022, 1, 17, 0, 2, 55, 465, DateTimeKind.Local).AddTicks(8914), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 2, 2, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") }
                });

            migrationBuilder.InsertData(
                table: "UserProjects",
                columns: new[] { "UserId", "ProjectId" },
                values: new object[,]
                {
                    { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a") },
                    { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a") },
                    { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("94e180fe-dc12-4d9a-bfc2-f562fc6d7e4a"), new Guid("f86a5963-0661-4d9c-aef1-e112dce86502") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("d1e9fb8e-9a25-41d1-844d-e14b4b1f6e41"), new Guid("0806c7f8-3e4d-484f-b931-5bb3c232a23d") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b") });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("6420c7df-f1f0-4e10-ad1b-8220834e44de"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("1f53c13c-05ca-4ea8-b5ec-addf4eb24e9c"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("476aa937-03e6-4a39-908d-4f526bb9054f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("49120adc-d7d3-489e-813a-3321133876d6"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("6481913d-1633-4778-8efe-249ac6f75679"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("731ebe18-53a5-4ea8-8e8c-f421791ae98e"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("83abb046-b780-4700-9898-f44d5f4cac95"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("90ed8e01-3005-4395-87b3-9ece3ebd6be9"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("d52662da-8429-46be-bcc8-141e875f8e96"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("dfbc2419-488d-4f64-aeb5-83bc2bf3ad50"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("e4787505-2f47-4f3e-9900-d3c99249e505"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("f01c4fac-09f8-4752-914a-91975b96779e"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("f885a95a-b573-4478-b7bf-c2c6e7cc7a71"));

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a") });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("0806c7f8-3e4d-484f-b931-5bb3c232a23d"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("f86a5963-0661-4d9c-aef1-e112dce86502"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("94e180fe-dc12-4d9a-bfc2-f562fc6d7e4a"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d1e9fb8e-9a25-41d1-844d-e14b4b1f6e41"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105"));

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "AspNetUsers",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("82a907f0-31be-454f-974d-8f6ba827ff84"), "9a137ba5-a1dc-4f94-9aff-4530072472cd", "Admin", "ADMIN" },
                    { new Guid("0dd79ff4-eb8d-4c2e-8ea9-f61272df1d67"), "567e5747-b760-4e66-9650-255be764dd98", "Manager", "MANAGER" },
                    { new Guid("6149e83b-d7d9-4842-ba59-08927f63094e"), "b1e24036-86a6-4b5a-8357-8e69706a1153", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), 0, "6b3d1b85-bd5d-484e-a542-44207a7660af", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEEx1KemNeFroTpmOScDdNdLSXD7QlThJLkZvcsceCiccfR9cGzcT++FDX22inQlDuQ==", null, false, "b6030cf1-5e56-40dc-b92c-43e34781b716", "Shoroh", false, "alex" },
                    { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), 0, "89bb3284-0e26-4476-9fef-1d34d9d79ba7", "nocka4real@gmail.com", true, "Danila", false, null, "NOCKA4REAL@GMAIL.COM", "NOCKA4REAL", "AQAAAAEAACcQAAAAEH7pYA81w+QboV2cnwFsDQlVRvx4mRUcnG1KnIz3WiYSTyoyGY/2erAnAbG+uKwoZw==", null, false, "3ae7ffb5-de2c-42c4-83c7-406bac81f88a", "Crazy", false, "nocka4real" },
                    { new Guid("ca484fd1-6cfb-488b-b6dd-51d185c5f0e7"), 0, "e2019942-548e-4491-8969-f4fa35ee6a10", "ns22091@gmail.com", true, "Nikita", false, null, "NS22091@GMAIL.COM", "NS22091", "AQAAAAEAACcQAAAAEPVY97hg1JMWAvEnpbTRqwJARiNgqBtbwhR6enAvCqpNLDNqyab88d9TfvJsoIGxPg==", null, false, "a1ae9676-4a54-4af5-8f7e-0a0134abd486", "Sidorov", false, "ns22091" },
                    { new Guid("70bc1287-f913-4135-8069-22d359955aef"), 0, "312e1681-ef51-4587-a63c-0e51f491582c", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAEAU+mGyY+NN2f1Bx7ZR3A4aJAqZciyxVLPpXL7i9e4UqbpszqSKlFSZ6mG7zhPRgxA==", null, false, "4c244f58-5120-4cf3-8854-d3de4e652c75", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("ea150e13-68b2-4576-b6c3-73c66531aa9b"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), new Guid("6149e83b-d7d9-4842-ba59-08927f63094e") },
                    { new Guid("ca484fd1-6cfb-488b-b6dd-51d185c5f0e7"), new Guid("0dd79ff4-eb8d-4c2e-8ea9-f61272df1d67") },
                    { new Guid("70bc1287-f913-4135-8069-22d359955aef"), new Guid("82a907f0-31be-454f-974d-8f6ba827ff84") },
                    { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("6149e83b-d7d9-4842-ba59-08927f63094e") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("6d6569d8-e3ea-4240-9e32-e9b64b559056"), new DateTime(2022, 1, 10, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5884), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 2, 6, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("f1d0a29b-d875-4f7d-81a9-f0f3c9640a09"), new DateTime(2022, 1, 5, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5876), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 4, 3, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("797e9e28-94d2-4e82-9086-e3befda6bdcc"), new DateTime(2022, 1, 9, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5872), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 4, 1, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("6824baa9-b9e1-4445-a51b-9a5ba97aa4a4"), new DateTime(2022, 1, 8, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5863), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 3, 5, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("3cccfa6c-b004-4fbb-8c0a-4185ae46d5cb"), new DateTime(2022, 1, 6, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5857), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 1, 1, new Guid("ddc59c19-361d-461b-9796-403e98bdacd4") },
                    { new Guid("33b74936-2eda-4ad4-afff-0519aebdf7b1"), new DateTime(2022, 1, 8, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5889), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033"), 3, 2, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("480fce6a-2047-4a81-9c0d-7dcb59ce537f"), new DateTime(2022, 1, 7, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5880), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 1, 2, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("3f2e2084-7ce5-4c2d-9fb0-2cf637e8a2f5"), new DateTime(2022, 1, 7, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5867), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 2, 4, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("78fd5b95-066d-4039-83f0-1b4d09e6f53f"), new DateTime(2022, 1, 10, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5845), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 2, 3, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("04f58ab3-97cb-4bc2-9a40-966a463e1838"), new DateTime(2022, 1, 5, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5837), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 3, 5, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("cc4dfff3-658d-44e8-93d1-3c44919622ce"), new DateTime(2022, 1, 7, 21, 49, 52, 880, DateTimeKind.Local).AddTicks(5748), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 1, 3, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") },
                    { new Guid("1d520c6c-2c5d-4f1f-b189-0f005a4b80cb"), new DateTime(2022, 1, 8, 21, 49, 52, 878, DateTimeKind.Local).AddTicks(6732), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729"), 2, 2, new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae") }
                });

            migrationBuilder.InsertData(
                table: "UserProjects",
                columns: new[] { "UserId", "ProjectId" },
                values: new object[,]
                {
                    { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729") },
                    { new Guid("ddc59c19-361d-461b-9796-403e98bdacd4"), new Guid("d537c698-3777-47e5-bc0c-1bcd8883c729") },
                    { new Guid("830251db-e21f-42a1-b2b4-a15aca6a9aae"), new Guid("3946feac-1fc7-4ff1-8d05-8b26b7b74033") }
                });
        }
    }
}
