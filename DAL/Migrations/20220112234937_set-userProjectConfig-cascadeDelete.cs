﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class setuserProjectConfigcascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("94e180fe-dc12-4d9a-bfc2-f562fc6d7e4a"), new Guid("f86a5963-0661-4d9c-aef1-e112dce86502") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("d1e9fb8e-9a25-41d1-844d-e14b4b1f6e41"), new Guid("0806c7f8-3e4d-484f-b931-5bb3c232a23d") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b") });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("6420c7df-f1f0-4e10-ad1b-8220834e44de"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("1f53c13c-05ca-4ea8-b5ec-addf4eb24e9c"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("476aa937-03e6-4a39-908d-4f526bb9054f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("49120adc-d7d3-489e-813a-3321133876d6"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("6481913d-1633-4778-8efe-249ac6f75679"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("731ebe18-53a5-4ea8-8e8c-f421791ae98e"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("83abb046-b780-4700-9898-f44d5f4cac95"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("90ed8e01-3005-4395-87b3-9ece3ebd6be9"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("d52662da-8429-46be-bcc8-141e875f8e96"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("dfbc2419-488d-4f64-aeb5-83bc2bf3ad50"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("e4787505-2f47-4f3e-9900-d3c99249e505"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("f01c4fac-09f8-4752-914a-91975b96779e"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("f885a95a-b573-4478-b7bf-c2c6e7cc7a71"));

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a") });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("0806c7f8-3e4d-484f-b931-5bb3c232a23d"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("f86a5963-0661-4d9c-aef1-e112dce86502"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("94e180fe-dc12-4d9a-bfc2-f562fc6d7e4a"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d1e9fb8e-9a25-41d1-844d-e14b4b1f6e41"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105"));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("4fdc78da-9aec-45c0-8e24-48c173fd8f68"), "d452b78d-ac87-4b0d-b982-381e3a7c3604", "Admin", "ADMIN" },
                    { new Guid("996b709b-0877-4a5c-acdb-5c2656c8b11f"), "c97a59f2-ccb5-4b13-9695-6a594ee56621", "Manager", "MANAGER" },
                    { new Guid("88368b5e-5392-4c8f-a953-b694402c0c6c"), "f823a88a-6c29-4e56-8c25-90fee5a80d71", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66"), 0, "59752de4-553f-481a-bdde-5477eb04982d", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEKVTEwOzOkuJASbFdMOESgC8+MwBItDREWLfXrlon5FSyH4bL6fv8FOONd9IN5Dmeg==", null, false, "40d0dd56-35e0-41ef-8c5e-93cd6de7f7ea", "Shoroh", false, "alex" },
                    { new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc"), 0, "478802a8-66d2-4a98-8d18-d768e71a295e", "serg.seoweb@gmail.com", true, "Serg", false, null, "SERG.SEOWEB@GMAIL.COM", "SERG.SEOWEB", "AQAAAAEAACcQAAAAENx5Y/158GFVqvE5DxG73KVgDUl+rO+xYK60sZEmMZ5fmoK0xp0HsQZ543545QsVYw==", null, false, "e82aafdf-da80-4491-a9ab-7245e2487c67", "Crazy", false, "serg.seoweb" },
                    { new Guid("22e6d0ff-881f-4ce8-b92f-6acca9a2e87c"), 0, "afa42222-af24-4137-84cd-9e361a4850a9", "ns22091@gmail.com", true, "Nikita", false, null, "NS22091@GMAIL.COM", "NS22091", "AQAAAAEAACcQAAAAEGKn2lKo6670qMja784RSBkEAGd1mSlkVuPevKCJ9ZtgdpC+PBbMhKPrx1ifpNyhTw==", null, false, "a7cbb6b1-0d81-4c32-86b0-dfb7d97c5db6", "Sidorov", false, "ns22091" },
                    { new Guid("9c5ebdc3-c636-4a56-aad4-e0508b0f7468"), 0, "70ef402e-26fe-4ef5-ad49-a5a613629395", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAEAwjajVkuNpDyNf/iTMoWs61Z6mvKHCnZ75iT2FXwkIxbaayW55JnHoiu27OPPOoPQ==", null, false, "d9b84d36-163a-44ba-87b4-b5ed560739fe", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("676106f7-2032-49b2-ab9f-df8d991eecf4"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("148dc260-1c2c-4137-be50-64a1c4413733"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc"), new Guid("88368b5e-5392-4c8f-a953-b694402c0c6c") },
                    { new Guid("22e6d0ff-881f-4ce8-b92f-6acca9a2e87c"), new Guid("996b709b-0877-4a5c-acdb-5c2656c8b11f") },
                    { new Guid("9c5ebdc3-c636-4a56-aad4-e0508b0f7468"), new Guid("4fdc78da-9aec-45c0-8e24-48c173fd8f68") },
                    { new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66"), new Guid("88368b5e-5392-4c8f-a953-b694402c0c6c") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("f4bfcda6-ac8d-4c30-b8c3-24df3e2df0d9"), new DateTime(2022, 1, 19, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4757), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 2, 6, new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc") },
                    { new Guid("9e825117-59de-44bc-a601-758119bdc137"), new DateTime(2022, 1, 14, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4748), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 4, 3, new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc") },
                    { new Guid("986f4a68-c23f-449b-8752-19c1618deeca"), new DateTime(2022, 1, 18, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4736), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 4, 1, new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc") },
                    { new Guid("0e69c9fa-d1fd-4df2-bd0f-c3d8caeb87a9"), new DateTime(2022, 1, 17, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4725), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 3, 5, new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc") },
                    { new Guid("9e798b32-757a-4fd8-8099-fe920989f272"), new DateTime(2022, 1, 15, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4719), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 1, 1, new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc") },
                    { new Guid("977713d8-0ab4-4c25-8c55-27bcd4bac000"), new DateTime(2022, 1, 17, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4761), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("148dc260-1c2c-4137-be50-64a1c4413733"), 3, 2, new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66") },
                    { new Guid("07ac0bb9-67fa-412f-86de-b3a617ed92be"), new DateTime(2022, 1, 16, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4753), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 1, 2, new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66") },
                    { new Guid("66cf2083-497b-4813-bd48-38761447bc14"), new DateTime(2022, 1, 16, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4730), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 2, 4, new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66") },
                    { new Guid("5b78a0c1-d283-465c-b22d-55ec75b08656"), new DateTime(2022, 1, 19, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4713), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 2, 3, new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66") },
                    { new Guid("abc644cb-b684-4814-8fe0-ae816bad1482"), new DateTime(2022, 1, 14, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4704), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 3, 5, new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66") },
                    { new Guid("98888477-76c0-4aea-8530-c642fc7b772f"), new DateTime(2022, 1, 16, 1, 49, 37, 401, DateTimeKind.Local).AddTicks(4625), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 1, 3, new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66") },
                    { new Guid("11ba3461-fb38-4733-a042-bdb016b9b64e"), new DateTime(2022, 1, 17, 1, 49, 37, 399, DateTimeKind.Local).AddTicks(5317), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"), 2, 2, new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66") }
                });

            migrationBuilder.InsertData(
                table: "UserProjects",
                columns: new[] { "UserId", "ProjectId" },
                values: new object[,]
                {
                    { new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66"), new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f") },
                    { new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc"), new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f") },
                    { new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66"), new Guid("148dc260-1c2c-4137-be50-64a1c4413733") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66"), new Guid("88368b5e-5392-4c8f-a953-b694402c0c6c") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("22e6d0ff-881f-4ce8-b92f-6acca9a2e87c"), new Guid("996b709b-0877-4a5c-acdb-5c2656c8b11f") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("9c5ebdc3-c636-4a56-aad4-e0508b0f7468"), new Guid("4fdc78da-9aec-45c0-8e24-48c173fd8f68") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc"), new Guid("88368b5e-5392-4c8f-a953-b694402c0c6c") });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("676106f7-2032-49b2-ab9f-df8d991eecf4"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("07ac0bb9-67fa-412f-86de-b3a617ed92be"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("0e69c9fa-d1fd-4df2-bd0f-c3d8caeb87a9"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("11ba3461-fb38-4733-a042-bdb016b9b64e"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("5b78a0c1-d283-465c-b22d-55ec75b08656"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("66cf2083-497b-4813-bd48-38761447bc14"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("977713d8-0ab4-4c25-8c55-27bcd4bac000"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("986f4a68-c23f-449b-8752-19c1618deeca"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("98888477-76c0-4aea-8530-c642fc7b772f"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("9e798b32-757a-4fd8-8099-fe920989f272"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("9e825117-59de-44bc-a601-758119bdc137"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("abc644cb-b684-4814-8fe0-ae816bad1482"));

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: new Guid("f4bfcda6-ac8d-4c30-b8c3-24df3e2df0d9"));

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66"), new Guid("148dc260-1c2c-4137-be50-64a1c4413733") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66"), new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f") });

            migrationBuilder.DeleteData(
                table: "UserProjects",
                keyColumns: new[] { "UserId", "ProjectId" },
                keyValues: new object[] { new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc"), new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f") });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("4fdc78da-9aec-45c0-8e24-48c173fd8f68"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("88368b5e-5392-4c8f-a953-b694402c0c6c"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("996b709b-0877-4a5c-acdb-5c2656c8b11f"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("22a61b08-ba8f-4d3f-997e-c030be63ac66"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("22e6d0ff-881f-4ce8-b92f-6acca9a2e87c"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("9c5ebdc3-c636-4a56-aad4-e0508b0f7468"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("cb236330-867e-4014-8c94-aeb8d66a37bc"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("148dc260-1c2c-4137-be50-64a1c4413733"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("93b68989-6730-4d6a-a5bf-419c307f0a8f"));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("0806c7f8-3e4d-484f-b931-5bb3c232a23d"), "c6dd210a-3c44-4c0b-bc7d-ea41471592ed", "Admin", "ADMIN" },
                    { new Guid("f86a5963-0661-4d9c-aef1-e112dce86502"), "1e8fd8da-f466-4f9b-8243-f524d00fbaf2", "Manager", "MANAGER" },
                    { new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b"), "40fca424-aceb-4a39-8704-4c662dcc3262", "Employee", "EMPLOYEE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Surname", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), 0, "d74d829b-fae4-4f9f-9d71-10f958f89b3f", "alex@gmail.com", true, "Alex", false, null, "ALEX@GMAIL.COM", "ALEX", "AQAAAAEAACcQAAAAEAe1omowPWWibaTl0ixaKo8cYxwVu6nuqwyslvV77MrNfO0HQTUid3fNjG6Sx2ElXg==", null, false, "c609d238-dbe7-47e4-934e-5515930a4c44", "Shoroh", false, "alex" },
                    { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), 0, "9e617fa8-59c5-4737-ac8c-886081e722c6", "serg.seoweb@gmail.com", true, "Serg", false, null, "SERG.SEOWEB@GMAIL.COM", "SERG.SEOWEB", "AQAAAAEAACcQAAAAEMbY6JooJRenAZ8peLb4RpeiggZAF/S9tvwapSAT+0+7BqIYo64kWOqvqghHoztZRQ==", null, false, "56477b0b-70c8-4b26-ad7e-c0f6f1737e43", "Crazy", false, "serg.seoweb" },
                    { new Guid("94e180fe-dc12-4d9a-bfc2-f562fc6d7e4a"), 0, "cd0ce4f6-cb94-466e-8511-1a86652128d2", "ns22091@gmail.com", true, "Nikita", false, null, "NS22091@GMAIL.COM", "NS22091", "AQAAAAEAACcQAAAAEMP6NXF1kwxtGRD+CR+nHnuH/Jr4E46bR+zptnyoUoBx7m3+0q18PdvIOCgaGdjknA==", null, false, "0642dd98-5d80-4fec-b425-9b8c2b0aec93", "Sidorov", false, "ns22091" },
                    { new Guid("d1e9fb8e-9a25-41d1-844d-e14b4b1f6e41"), 0, "a536e479-b144-400e-8897-830fab014943", "serg@gmail.com", true, "Serg", false, null, "SERG@GMAIL.COM", "SERG", "AQAAAAEAACcQAAAAEAGpyAWYGsm6g8dWgpwcM3EpHZ92wlHbs1VgZG8fRDlOlOfTAwcZlmH4OcKjqOafyQ==", null, false, "8455e99d-1f88-4168-8782-b86470f79a17", "Simson", false, "serg" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "IsCompleted", "IsPublished", "Name" },
                values: new object[,]
                {
                    { new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 1" },
                    { new Guid("6420c7df-f1f0-4e10-ad1b-8220834e44de"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 2" },
                    { new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", false, false, "Project 3" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b") },
                    { new Guid("94e180fe-dc12-4d9a-bfc2-f562fc6d7e4a"), new Guid("f86a5963-0661-4d9c-aef1-e112dce86502") },
                    { new Guid("d1e9fb8e-9a25-41d1-844d-e14b4b1f6e41"), new Guid("0806c7f8-3e4d-484f-b931-5bb3c232a23d") },
                    { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("c37d571f-20b4-481c-be2c-c38d2b47850b") }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Deadline", "Description", "Name", "ProjectId", "TaskPriority", "TaskStatus", "UserId" },
                values: new object[,]
                {
                    { new Guid("d52662da-8429-46be-bcc8-141e875f8e96"), new DateTime(2022, 1, 19, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8912), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 11", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 2, 6, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("6481913d-1633-4778-8efe-249ac6f75679"), new DateTime(2022, 1, 14, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8903), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 9", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 4, 3, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("1f53c13c-05ca-4ea8-b5ec-addf4eb24e9c"), new DateTime(2022, 1, 18, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8899), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 8", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 4, 1, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("476aa937-03e6-4a39-908d-4f526bb9054f"), new DateTime(2022, 1, 17, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8890), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 6", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 3, 5, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("f885a95a-b573-4478-b7bf-c2c6e7cc7a71"), new DateTime(2022, 1, 15, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8884), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 5", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 1, 1, new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945") },
                    { new Guid("83abb046-b780-4700-9898-f44d5f4cac95"), new DateTime(2022, 1, 17, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8916), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 12", new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105"), 3, 2, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("e4787505-2f47-4f3e-9900-d3c99249e505"), new DateTime(2022, 1, 16, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8907), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 10", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 1, 2, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("49120adc-d7d3-489e-813a-3321133876d6"), new DateTime(2022, 1, 16, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8894), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 7", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 2, 4, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("90ed8e01-3005-4395-87b3-9ece3ebd6be9"), new DateTime(2022, 1, 19, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8869), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 4", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 2, 3, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("dfbc2419-488d-4f64-aeb5-83bc2bf3ad50"), new DateTime(2022, 1, 14, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8861), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 3", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 3, 5, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("f01c4fac-09f8-4752-914a-91975b96779e"), new DateTime(2022, 1, 16, 0, 2, 55, 467, DateTimeKind.Local).AddTicks(8766), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 2", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 1, 3, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") },
                    { new Guid("731ebe18-53a5-4ea8-8e8c-f421791ae98e"), new DateTime(2022, 1, 17, 0, 2, 55, 465, DateTimeKind.Local).AddTicks(8914), "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Task 1", new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a"), 2, 2, new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c") }
                });

            migrationBuilder.InsertData(
                table: "UserProjects",
                columns: new[] { "UserId", "ProjectId" },
                values: new object[,]
                {
                    { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a") },
                    { new Guid("ec00d99f-f373-4aa2-8f04-7b3ac8607945"), new Guid("1cfb1da7-8fdd-485d-b1e9-e3395c5c665a") },
                    { new Guid("9563ecce-7b51-4ed7-abde-1a2be43c5c8c"), new Guid("35a23bd7-458b-45d7-98a7-e00f64f7b105") }
                });
        }
    }
}
