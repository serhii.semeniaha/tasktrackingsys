﻿using DAL.Entities;

namespace DAL.Repo.Interfaces
{
  public interface IProjectRepo : IGenericRepo<Project>
  {
  }
}