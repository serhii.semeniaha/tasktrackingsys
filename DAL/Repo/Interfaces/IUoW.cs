﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace DAL.Repo.Interfaces
{
  public interface IUoW
  {
    public IProjectRepo ProjectRepo { get; }
    public ITaskRepo TaskRepo { get; }
    UserManager<User> UserManager { get; }
    public Task<bool> SaveAsync();
  }
}
