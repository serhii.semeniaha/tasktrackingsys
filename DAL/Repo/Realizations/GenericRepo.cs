﻿using DAL.Repo.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repo.Realizations
{
  internal abstract class GenericRepo<T> : IGenericRepo<T> where T : Entities.BaseEntity
  {
    protected DbContext Context { get; }
    protected DbSet<T> DbSet { get; }
    protected GenericRepo(DbContext context)
    {
      Context = context;
      DbSet = context.Set<T>();
    }

    public IQueryable<T> FindAll()
    {
      return this.DbSet.AsNoTracking();
    }

    public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
    {
      return this.DbSet.Where(expression).AsNoTracking();
    }

    public async Task CreateAsync(T entity)
    {
      await this.DbSet.AddAsync(entity);
    }

    public void Delete(T entity)
    {
      this.DbSet.Remove(entity);
    }

    public void Attach(T entity)
    {
      this.DbSet.Attach(entity);
    }
    public void Update(T entity)
    {
      this.DbSet.Update(entity);
    }

    public IQueryable<T> Include(params Expression<Func<T, object>>[] includes)
    {
      var query = DbSet.AsQueryable();
      if (includes != null)
        includes.ToList().ForEach(i =>
        {
          if (i != null)
            query = query.Include(i);
        });
      return query;
    }

    public async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> predicate = null)
    {
      return await this.GetQuery(predicate).ToListAsync();
    }

    public async Task<T> GetFirstOrDefaultAsync(Expression<Func<T, bool>> predicate = null)
    {
      return await this.GetQuery(predicate).FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<T>> GetAllWithIncludingAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
    {
      return await this.GetQueryAsTracking(predicate, include).ToListAsync();
    }

    public async Task<T> GetFirstOrDefaultWithIncludingAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
    {
      return await this.GetQueryAsTracking(predicate, include).FirstOrDefaultAsync();
    }

    private IQueryable<T> GetQuery(Expression<Func<T, bool>> predicate = null)
    {
      var query = this.DbSet.AsNoTracking();

      if (predicate != null)
      {
        query = query.Where(predicate);
      }
      return query;
    }
    private IQueryable<T> GetQueryAsTracking(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
    {
      var query = this.DbSet.AsTracking();
      if (include != null)
      {
        query = include(query);
      }
      if (predicate != null)
      {
        query = query.Where(predicate);
      }
      return query;
    }

  }
}
