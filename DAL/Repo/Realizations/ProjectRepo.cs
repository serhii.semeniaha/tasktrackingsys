﻿using DAL.Entities;
using DAL.Repo.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repo.Realizations
{
  internal class ProjectRepo : GenericRepo<Project>, IProjectRepo
  {
    public ProjectRepo(DbContext context) : base(context)
    {
    }
  }
}
