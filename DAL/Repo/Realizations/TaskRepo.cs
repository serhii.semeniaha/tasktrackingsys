﻿using DAL.Entities;
using DAL.Repo.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repo.Realizations
{
  internal class TaskRepo : GenericRepo<Taskk>, ITaskRepo
  {
    public TaskRepo(DbContext context) : base(context)
    {
    }
  }
}
