﻿using DAL.Entities;
using DAL.Repo.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Repo.UoW
{
  public class UoW : IUoW
  {
    private readonly DbContext _context;
    public IProjectRepo ProjectRepo { get; }
    public ITaskRepo TaskRepo { get; }
    public UserManager<User> UserManager { get; }

    public UoW(
      DbContext context,
      IProjectRepo projectRepo,
      ITaskRepo taskRepo,
      UserManager<User> userManager)
    {
      _context = context;
      ProjectRepo = projectRepo;
      TaskRepo = taskRepo;
      UserManager = userManager;
    }

    public async Task<bool> SaveAsync()
    {
      return await _context.SaveChangesAsync() > 0;
    }
  }
}
