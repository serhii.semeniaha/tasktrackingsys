import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserToProject } from '../shared/models/user/user-to-project.model';
import { Project } from '../shared/models/project/project.model';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }

  formData: Project = new Project();
  list!: Project[];

  postProject() {
    return this.http.post('/api/project', this.formData);
  }

  putProject(){
    return this.http.put('/api/project', this.formData)
  }

  deleteProject(id:string){
    return this.http.delete(`/api/project/${id}`)
  }

  deleteUserFromProject(userToProject:UserToProject){
    return this.http.post('/api/project/remove-user', userToProject)
  }

  addUserToProject(userToProject:UserToProject){
    return this.http.post('/api/project/add-user', userToProject)
  }

  refreshListbyUser() {
    this.http.get('/api/project/by-user').toPromise()
      .then(res => this.list = res as Project[]);
  }

  refreshList(){
    this.http.get('/api/project').toPromise()
    .then(res => this.list = res as Project[]);
  }
}
