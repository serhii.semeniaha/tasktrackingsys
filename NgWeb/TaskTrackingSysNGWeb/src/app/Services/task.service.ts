import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UserOfProject} from '../shared/models/user/user-of-project.model';
import {Task} from '../shared/models/task/task.model';
import {TasksOfProject} from '../shared/models/task/tasks-of-project.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) {
  }

  formData: Task = new Task();
  list!: Task[];
  percentageCompletion: number = 0
  listOfProjectUsers!: UserOfProject[];

  refreshListManager(projectId: string) {
    this.http.get(`/api/task/project/${projectId}`).toPromise()
      .then(res => {
        let data = res as TasksOfProject
        this.list = data.tasks
        this.percentageCompletion = data.percentageCompletion
      });
    this.http.get(`api/user/project-users/${projectId}`).toPromise()
      .then(res => this.listOfProjectUsers = res as UserOfProject[]);
  }

  refreshListUser(projectId: string) {
    this.http.get(`/api/task/project/${projectId}`).toPromise()
      .then(res => {
        let data = res as TasksOfProject
        this.list = data.tasks
        this.percentageCompletion = data.percentageCompletion
        console.log(res);
      });

  }

  deleteTask(id: string) {
    return this.http.delete(`/api/task/${id}`)
  }

  postTask() {
    return this.http.post('/api/task', this.formData);
  }

  putTask() {
    if (this.formData.userId == "null")
      this.formData.userId = null;
    return this.http.put('/api/task', this.formData);
  }
}
