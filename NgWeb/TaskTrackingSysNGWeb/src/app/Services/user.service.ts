import {Injectable} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {HttpClient} from "@angular/common/http";
import {UserInProject} from '../shared/models/user/user-in-project.model';
import {UserRole} from '../shared/models/user/user-role.model';
import {RoleEnum} from "../shared/models/user/roleEnum.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  role: string | undefined
  listOfUsersInProject!: UserInProject[];
  listOfUsersAndManagers!: UserRole[];
  userRoleFormData: UserRole = new UserRole();

  constructor(private fb: FormBuilder, private http: HttpClient) {
  }


  changeUserRole() {
    return this.http.post('/api/user/set-user-role', this.userRoleFormData);
  }

  usersInProject(projectId: string) {
    return this.http.get(`api/user/users-with-project/${projectId}`).toPromise()
      .then(res => this.listOfUsersInProject = res as UserInProject[]);
  }

  usersAndManagers() {
    return this.http.get('api/user/users-and-managers').toPromise()
      .then(res => this.listOfUsersAndManagers = res as UserRole[]);
  }

  removeUser(userId: string) {
    return this.http.delete(`/api/user/${userId}`);
  }

  defineRole() {
    if (localStorage.getItem('token') !== null) {
      let userRole = JSON.parse(window.atob(localStorage.getItem('token')?.split('.')[1] || ''));
      if (this.roleMatch([RoleEnum.Admin])) this.role = RoleEnum.Admin
      if (this.roleMatch([RoleEnum.Manager])) this.role = RoleEnum.Manager
      if (this.roleMatch([RoleEnum.Employee])) this.role = RoleEnum.Employee
      console.log(userRole);
    }

  }

  roleMatch(allowedRoles: any[]): boolean {
    let isMatch = false;
    let tokenObj = JSON.parse(window.atob(localStorage.getItem('token')?.split('.')[1] || ''));
    let userRole = tokenObj["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
    allowedRoles.forEach(element => {
      if (userRole == element) {
        isMatch = true;
      }
    });
    return isMatch;
  }
}
