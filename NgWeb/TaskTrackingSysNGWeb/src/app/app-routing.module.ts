import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './shared/helpers/auth/auth.guard';
import {HomeComponent} from "./components/home/home.component";
import {ForbiddenComponent} from "./components/error-pages/forbidden/forbidden.component";
import {ProjectUsersComponent} from './components/project/project-users/project-users.component';
import {ProjectTasksComponent} from './components/project/project-tasks/project-tasks.component';
import {TaskInfoComponent} from "./components/project/project-tasks/task-info/task-info.component";
import {RoleEnum} from "./shared/models/user/roleEnum.model";

const routes: Routes = [
  {path: '', redirectTo: '/auth', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {
    path: 'project/:id/users',
    component: ProjectUsersComponent,
    canActivate: [AuthGuard],
    data: {permittedRoles: [RoleEnum.Manager]}
  },
  {
    path: 'project/:id/tasks',
    component: ProjectTasksComponent,
    canActivate: [AuthGuard],
    data: {permittedRoles: [RoleEnum.Employee, RoleEnum.Manager]}
  },
  {
    path: 'project/:id/tasks',
    component: TaskInfoComponent,
    canActivate: [AuthGuard],
    data: {permittedRoles: [RoleEnum.Employee, RoleEnum.Manager]}
  },
  {path: 'forbidden', component: ForbiddenComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
