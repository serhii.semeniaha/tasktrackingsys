import {Component, OnInit} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {LoginService} from "./auth/services/login.service";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {UserService} from "./Services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Task Tracking System';

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.defineRole();
  }
}
