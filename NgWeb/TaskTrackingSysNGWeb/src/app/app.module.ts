import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AuthModule} from "./auth/auth.module";
import {HomeComponent} from "./components/home/home.component";
import {NavComponent} from "./components/nav/nav.component";
import {ForbiddenComponent} from "./components/error-pages/forbidden/forbidden.component";
import {ProjectComponent} from "./components/project/project.component";
import {AdminPanelComponent} from "./components/admin-panel/admin-panel.component";
import {ProjectUsersComponent} from "./components/project/project-users/project-users.component";
import {ProjectTasksComponent} from "./components/project/project-tasks/project-tasks.component";
import {ProjectInfoComponent} from "./components/project/project-info/project-info.component";
import {UserRoleComponent} from "./components/admin-panel/user-role/user-role.component";
import {TaskInfoComponent} from "./components/project/project-tasks/task-info/task-info.component";
import {SortDirective} from "./shared/helpers/util/sort.directive";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import {Ng2SearchPipeModule} from "ng2-search-filter";
import {UserService} from "./Services/user.service";
import { AuthInterceptor } from './shared/helpers/auth/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    ForbiddenComponent,
    ProjectComponent,
    ProjectUsersComponent,
    ProjectTasksComponent,
    TaskInfoComponent,
    ProjectInfoComponent,
    AdminPanelComponent,
    UserRoleComponent,
    SortDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      progressBar: true
    }),
    Ng2SearchPipeModule
  ],
  providers: [
    UserService, {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
