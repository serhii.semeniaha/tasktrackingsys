import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

import {RegisterComponent} from './register/register.component';
import {AuthComponent} from './auth.component';
import {LoginComponent} from './login/login.component';


const routes: Routes = [
  {
    path: 'auth',
    component: AuthComponent
  }
]

@NgModule({
  declarations: [
    RegisterComponent,
    AuthComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ]
})
export class AuthModule {
}
