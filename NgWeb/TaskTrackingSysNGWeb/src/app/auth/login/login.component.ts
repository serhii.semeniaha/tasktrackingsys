import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from "@angular/forms";
import {LoginService} from "../services/login.service";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {UserService} from "../../Services/user.service";

@Component({
  selector: 'tts-child-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(private fb: FormBuilder, private loginService: LoginService, private router: Router, private toastr: ToastrService, private userService: UserService) { }

  ngOnInit(): void {
    this.initializeLoginForm();
  }

  initializeLoginForm(): void{
    this.loginForm = this.fb.group({
      login: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    })
  }

  get login(): FormControl{
    return this.loginForm?.get('login') as FormControl
  }
  get password(): FormControl{
    return this.loginForm?.get('password') as FormControl
  }

  signIn(): void{
    if(this.loginForm?.valid){
      this.loginService.login(this.login.value, this.password.value).subscribe(
        (res: any) => {
          localStorage.setItem('token', res.token);
          this.userService.defineRole();
          this.router.navigateByUrl('/home');
        },
        err => {
          if (err.status == 400){
            this.toastr.error(err.error, 'Authentication failed.');
          }
          else
            console.log(err);
        });
    }
  }
}
