import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from "@angular/forms";
import {RegisterService} from "../services/register.service";
import {UserRegister} from "../../shared/models/user/user-register.model";
import {v4 as uuidv4} from 'uuid';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Component({
  selector: 'tts-child-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  regForm!: FormGroup;

  constructor(private fb: FormBuilder, private registerService: RegisterService, private toastr: ToastrService, private router: Router) {
  }

  ngOnInit(): void {
    this.initializeRegForm();
  }

  initializeRegForm(): void {
    this.regForm = this.fb.group({
      regFirstName: ['',
        [Validators.required, Validators.pattern("^[a-zA-Z0-9]{2,25}((\s+|-)[a-zA-Z0-9]{2,25})*$")]
      ],
      regLastName: ['',
        [Validators.required, Validators.pattern("^[a-zA-Z0-9]{2,25}((\s+|-)[a-zA-Z0-9]{2,25})*$")]
      ],
      regEmail: ['',
        [Validators.required, Validators.email]
      ],
      regPassword: ['',
        [Validators.required, Validators.pattern("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,25}$")]
      ],
      confirmPassword: ['', [Validators.required] ]
    }, {
      validators: this.MustMatch('regPassword', 'confirmPassword')
    })
  }

  get regFirstName(): FormControl{
    return this.regForm?.get('regFirstName') as FormControl
  }

  get regLastName(): FormControl{
    return this.regForm?.get('regLastName') as FormControl
  }

  get regEmail(): FormControl{
    return this.regForm?.get('regEmail') as FormControl
  }

  get regPassword(): FormControl{
    return this.regForm?.get('regPassword') as FormControl
  }

  get confPassword(): FormControl{
    return this.regForm?.get('confirmPassword') as FormControl
  }

  signUp(): void{
    if(this.regForm?.valid){
      let user = new UserRegister();
      user.Id = uuidv4();
      user.FirstName = this.regFirstName.value;
      user.SurName = this.regLastName.value;
      user.Email = this.regEmail.value;
      user.UserName = this.regEmail.value.split('@', 2)[0];
      user.Password = this.regPassword.value;
      this.registerService.register(user).subscribe(
        (res: any) => {
          this.regForm.reset();
          this.toastr.success('New user created! Now you can try to log in', 'Registration successful.');
          this.router.navigateByUrl('/auth');
        },
        err => {
          if (err.status == 400){
            this.toastr.error(err.error, 'Registration failed.');
          }
          else
            console.log(err);
        });
    }
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (fg: FormGroup) => {
      const control = fg.controls[controlName];
      const matchingControl = fg.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({mustMatch: true});
      } else {
        matchingControl.setErrors(null);
      }
    }
  }
}

