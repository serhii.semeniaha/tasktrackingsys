import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserRegister} from "../../shared/models/user/user-register.model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(login: string, password: string): Observable<any> {
    return this.http.post('/api/auth/login', { login, password });
  }
}
