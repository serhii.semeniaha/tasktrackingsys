import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserRegister} from "../../shared/models/user/user-register.model";

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  register(body: UserRegister): Observable<any> {
    return this.http.post('/api/auth/register', body);
  }
}
