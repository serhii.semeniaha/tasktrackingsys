import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'tts-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  isLogged: boolean = false;
  constructor(private router: Router, public userService: UserService) { }

  ngOnInit() {
  }

}
