import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'tts-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})

export class NavComponent implements OnInit {
  isLogged : boolean | undefined
  role!: string | null;

  constructor(private router: Router, public service: UserService) {
  }

  ngOnInit() {
  }

  onLogout() {
    localStorage.removeItem('token');
    this.service.role = undefined
    this.router.navigate(['/auth']);
  }
}
