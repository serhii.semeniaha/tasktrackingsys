import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Project } from 'src/app/shared/models/project/project.model';
import { ProjectService } from 'src/app/Services/project.service';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'tts-project',
  templateUrl: './project.component.html',
  styles: [
  ]
})
export class ProjectComponent implements OnInit {

  serach:string = '';
  constructor(public projectService:ProjectService, public userService:UserService, private toastr:ToastrService) { }

  ngOnInit(): void {
    if(this.userService.role === 'Employee')
      this.projectService.refreshListbyUser();
    else
      this.projectService.refreshList();
    this.serach ='';
  }

  populateForm(selectedRecord:Project){
    this.projectService.formData =  Object.assign({}, selectedRecord);
  }

  onDelete(project: Project){
    if (confirm('Are you sure to delete this record?'))
    {
      this.projectService.deleteProject(project.id)
      .subscribe(
       res=>{
         this.projectService.refreshList();
         this.projectService.formData = new Project();
         this.toastr.error("Deleted successfully", `Project '${project.name}' deleted`)
       },
       err => {
        this.toastr.error(err.error, "Project wasn't deleted")
        }
      )
    }
  }
}
