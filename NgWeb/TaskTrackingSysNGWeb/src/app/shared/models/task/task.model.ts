import {DatePipe} from "@angular/common"

export class Task {
  id: string = ''
  name: string = ''
  description: string = ''
  creationDate: string | null = ''
  lastUpdate: string | null = ''
  deadline: string | null
  taskStatus: string = ''
  taskStatusId: number = 1
  taskPriority: string = ''
  taskPriorityId: number = 1
  projectId: string = ''
  userId!: string | null

  constructor() {
    const datePipe = new DatePipe('en-US');
    let date = new Date();
    this.deadline = datePipe.transform(date, 'yyyy-MM-dd');
  }
}
