import {Task} from "./task.model"

export class TasksOfProject {
  percentageCompletion: number = 0
  tasks: Task[] = []
}
