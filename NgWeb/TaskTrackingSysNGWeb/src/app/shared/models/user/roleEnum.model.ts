export enum RoleEnum {
  Employee = 'Employee',
  Admin = 'Admin',
  Manager = 'Manager'
}
