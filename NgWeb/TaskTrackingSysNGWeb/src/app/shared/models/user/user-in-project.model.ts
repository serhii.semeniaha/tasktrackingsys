export class UserInProject {
  userId: string = '';
  fullName: string = '';
  isWithProject: boolean = false;
}
