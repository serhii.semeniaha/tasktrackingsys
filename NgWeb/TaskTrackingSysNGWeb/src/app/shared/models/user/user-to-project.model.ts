export class UserToProject {
  projectId: string = ''
  userId: string = ''

  constructor(prId: string, uId: string) {
    this.projectId = prId
    this.userId = uId
  }
}
