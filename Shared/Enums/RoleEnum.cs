﻿namespace Shared.Enums
{
  public enum RoleEnum
  {
    Admin = 1,
    Manager,
    Employee
  }
}
