﻿namespace Shared.Enums
{
  public enum TaskStatus
  {
    Scheduled = 1,
    Pending,
    In_Progress,
    Review,
    Completed,
    Cancelled
  }
}
