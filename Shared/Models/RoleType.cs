﻿using System.Collections.Generic;

namespace Shared.Models
{
  public static class RoleType
  {
    public const string ADMIN = "Admin";
    public const string MANAGER = "Manager";
    public const string EMPLOYEE = "Employee";

    public static List<string> ListOfRoles = new List<string>
    {
      RoleType.ADMIN,
      RoleType.MANAGER,
      RoleType.EMPLOYEE
    };
  }
}
