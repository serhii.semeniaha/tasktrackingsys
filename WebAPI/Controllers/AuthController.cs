﻿using BLL.DTOs.User;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
  /// <summary>
  /// Contains http methods for authentication and registration
  /// </summary>

  // api/auth
  [Route("api/[controller]")]
  [ApiController]
  public class AuthController : ControllerBase
  {
    private readonly ILoggerService<AuthController> _logger;
    private readonly IAuthService _authService;

    public AuthController(ILoggerService<AuthController> logger, IAuthService authService)
    {
      _logger = logger;
      _authService = authService;
    }

    /// <summary>
    /// Method for logging in
    /// </summary>
    /// <response code="200">Returns token</response>

    // api/auth/login
    [HttpPost("login")]
    public async Task<ActionResult> Login(LoginDto loginDto)
    {
      _logger.LogInformation($"User - {loginDto.Login}, trying to sign in");

      var token = await _authService.SignInAsync(loginDto);

      _logger.LogInformation($"User - {loginDto.Login}, signed in successfully");

      return Ok(new { token });
    }


    /// <summary>
    /// Method for registration
    /// </summary>
    /// <response code="204">Returns nothing, registration was successful</response>

    // api/auth/register
    [HttpPost("register")]
    [ProducesResponseType(204)]
    public async Task<ActionResult> Register(RegistrationDto registrationDto)
    {
      var signedUpUser = await _authService.SignUpAsync(registrationDto);

      _logger.LogInformation($"User - {signedUpUser.Email}, signed up successfully");

      return NoContent();
    }
  }
}
