﻿using BLL.DTOs.Project;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Models;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
  /// <summary>
  /// Contains REST API methods for working with projects
  /// </summary>
  // api/project
  [Route("api/[controller]")]
  [ApiController]
  [Authorize(AuthenticationSchemes = "Bearer")]
  public class ProjectController : ControllerBase
  {
    private readonly IEmailService _emailService;
    private readonly ILoggerService<ProjectController> _logger;
    private readonly IProjectService _projectService;

    public ProjectController(IEmailService emailService, ILoggerService<ProjectController> logger, IProjectService projectService)
    {
      _emailService = emailService;
      _logger = logger;
      _projectService = projectService;
    }

    /// <summary>
    /// Method returns project that was created and path to it
    /// </summary>
    /// <response code="201">Returns project that was created and path to it</response>

    //POST api/project
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpPost]
    [ProducesResponseType(typeof(ProjectDto), 201)]
    public async Task<IActionResult> CreateProject(ProjectDto projectDto)
    {
      _logger.LogInformation($"Start creating project - {projectDto.Id}");
      var project = await _projectService.CreateAsync(projectDto);
      _logger.LogInformation($"Project with id {projectDto.Id} was created successfully");

      return CreatedAtRoute("GetProjectById", new { id = project.Id }, project);
    }

    /// <summary>
    /// Method returns all projects
    /// </summary>
    /// <response code="200">Returns all projects</response>

    //GET api/project
    [Authorize(Roles = RoleType.ADMIN + "," + RoleType.MANAGER)]
    [HttpGet]
    public async Task<ActionResult> GetAllProjects()
    {
      var projects = await _projectService.GetAllProjectsAsync();

      return Ok(projects);
    }


    /// <summary>
    /// Method returns all projects for current user
    /// </summary>
    /// <response code="200">Returns all projects for current user</response>
    /// <response code="404">Returns message that nothing was found</response>

    //GET api/project/by-user
    [HttpGet("by-user")]
    public async Task<IActionResult> GetAllProjectsByUser()
    {
      var projects = await _projectService.GetAllProjectsByUserIdAsync(Guid.Parse(User.FindFirstValue("UserId")));

      if (!projects.Any())
        return NotFound();

      return Ok(projects);
    }


    /// <summary>
    /// Method returns project that has an inputted Id property
    /// </summary>
    /// <response code="200">Returns project that has an inputted Id property</response>

    //GET api/project/{id}
    [HttpGet("{id:Guid}", Name = "GetProjectById")]
    public async Task<IActionResult> GetProjectById(Guid id)
    {
      var project = await _projectService.GetByIdAsync(id);

      return Ok(project);
    }


    /// <summary>
    /// Method changes project
    /// </summary>
    /// <response code="204">Returns nothing, project was successfully changed</response>

    //PUT api/project
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpPut]
    [ProducesResponseType(204)]
    public async Task<IActionResult> UpdateProject(ProjectDto projectDto)
    {
      _logger.LogInformation($"Start updating project with id {projectDto.Id}");

      await _projectService.UpdateAsync(projectDto);

      _logger.LogInformation($"Project with id {projectDto.Id} was updated successfully");

      return NoContent();
    }


    /// <summary>
    /// Method removes project
    /// </summary>
    /// <response code="204">Returns nothing, project was successfully removed</response>
    /// <response code="404">Returns message that project was not found</response>

    //DELETE api/project/{id}
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpDelete("{id:Guid}")]
    [ProducesResponseType(204)]
    public async Task<IActionResult> RemoveProject(Guid id)
    {
      _logger.LogInformation($"Start removing project with id {id}");

      await _projectService.RemoveAsync(id);
      _logger.LogInformation($"Project with id {id} and all its tasks were removed successfully");

      return NoContent();
    }


    /// <summary>
    /// Method adds user to project and sends email to this person
    /// </summary>
    /// <response code="204">Returns nothing, user was successfully added to project</response>

    //POST api/project/add-user
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpPost("add-user")]
    [ProducesResponseType(204)]
    public async Task<IActionResult> AddUserToProject(UserProjectDto userProjectDto)
    {
      _logger.LogInformation(
          $"Manager wants to add user with Id {userProjectDto.UserId} to project with Id {userProjectDto.ProjectId}");

      await _projectService.AddUserToProjectAsync(userProjectDto);
      _logger.LogInformation(
          $"Manager added user with Id {userProjectDto.UserId} to project with Id {userProjectDto.ProjectId} successfully");

      await _emailService.SendEmailAboutNewProjectAsync(userProjectDto);

      return NoContent();
    }


    /// <summary>
    /// Method remove user from project and sends email to this person
    /// </summary>
    /// <response code="204">Returns nothing, user was successfully removed from project</response>

    //POST api/project/remove-user
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpPost("remove-user")]
    [ProducesResponseType(204)]
    public async Task<IActionResult> RemoveUserToProject(UserProjectDto userProjectDto)
    {
      _logger.LogInformation(
          $"Start removing user with Id {userProjectDto.UserId} from project with Id {userProjectDto.ProjectId}");

      await _projectService.RemoveUserFromProjectAsync(userProjectDto);
      _logger.LogInformation(
          $"User with Id {userProjectDto.UserId} was removed from project with Id {userProjectDto.ProjectId} successfully");

      return NoContent();
    }
  }
}
