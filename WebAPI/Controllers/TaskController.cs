﻿using BLL.DTOs.Task;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Models;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
  /// <summary>
  /// Contains REST API methods for working with tasks
  /// </summary>
  // api/task
  [Route("api/[controller]")]
  [ApiController]
  [Authorize(AuthenticationSchemes = "Bearer")]
  public class TaskController : ControllerBase
  {
    private readonly IEmailService _emailService;
    private readonly ILoggerService<TaskController> _logger;
    private readonly ITaskService _taskService;

    public TaskController(IEmailService emailService, ILoggerService<TaskController> logger, ITaskService taskService)
    {
      _emailService = emailService;
      _logger = logger;
      _taskService = taskService;
    }


    /// <summary>
    /// Method returns task by Id property
    /// </summary>
    /// <response code="200">Returns task that has an inputted Id property</response>

    //GET api/tasks/{id}
    [HttpGet("{id:Guid}", Name = "GetTaskById")]
    public async Task<IActionResult> GetTaskById(Guid id)
    {
      var task = await _taskService.GetByIdAsync(id);

      return Ok(task);
    }


    /// <summary>
    /// Method returns task that was created and the path to it
    /// </summary>
    /// <response code="201">Returns task that was created and path to it</response>

    //POST api/tasks
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpPost]
    [ProducesResponseType(typeof(TaskDto), 201)]
    public async Task<IActionResult> CreateTask(TaskDto taskDto)
    {

      _logger.LogInformation($"Start creating a task - {taskDto.Id}");

      var createdTask = await _taskService.CreateAsync(taskDto);
      _logger.LogInformation($"Task with id {taskDto.Id} was created successfully");
      if (createdTask.UserId != null)
        await _emailService.SendEmailAboutNewTaskAsync(createdTask);

      return CreatedAtRoute("GetTaskById", new { id = createdTask.Id }, createdTask);
    }


    /// <summary>
    /// Method changes task
    /// </summary>
    /// <response code="204">Returns nothing, task was successfully changed</response>

    //PUT api/tasks
    [Authorize(Roles = RoleType.EMPLOYEE + "," + RoleType.MANAGER)]
    [HttpPut]
    [ProducesResponseType(204)]
    public async Task<IActionResult> UpdateTask(TaskDto taskDto)
    {
      var userId = Guid.Parse(User.FindFirstValue("UserId"));
      _logger.LogInformation($"Start updating a task - {taskDto.Id}");

      await _taskService.UpdateAsync(taskDto, userId);

      _logger.LogInformation($"Task with id {taskDto.Id} was updated successfully");

      return NoContent();
    }


    /// <summary>
    /// Method returns tasks by ProjectId property
    /// </summary>
    /// <response code="200">Returns tasks and percentage of completion that has an inputted ProjectId property</response>
    /// <response code="404">Returns message that nothing was found</response>

    //GET api/tasks/project/{id}
    [Authorize(Roles = RoleType.EMPLOYEE + "," + RoleType.MANAGER)]
    [HttpGet("project/{id:Guid}")]
    public async Task<IActionResult> GetTasksByProjectId(Guid id)
    {
      var userId = new Guid(User.FindFirstValue("UserId"));

      var tasks = await _taskService.GetTasksByProjectIdAsync(id, userId);

      if (!tasks.Tasks.Any())
        return NotFound();

      return Ok(tasks);
    }


    /// <summary>
    /// Method removes task
    /// </summary>
    /// <response code="204">Returns nothing, task was successfully removed</response>
    /// <response code="404">Returns message that task was not found</response>

    //DELETE api/tasks/{id}
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpDelete("{id:Guid}")]
    [ProducesResponseType(204)]
    public async Task<IActionResult> RemoveTask(Guid id)
    {
      _logger.LogInformation($"Start removing a task - {id}");

      await _taskService.RemoveAsync(id);
      _logger.LogInformation($"Task with id {id} was removed successfully");

      return NoContent();
    }
  }
}
