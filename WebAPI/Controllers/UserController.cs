﻿using BLL.DTOs.User;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
  /// <summary>
  /// Contains REST API methods for working with user
  /// </summary>
  // api/user
  [Route("api/[controller]")]
  [ApiController]
  [Authorize(AuthenticationSchemes = "Bearer")]
  public class UserController : ControllerBase
  {
    private readonly IEmailService _emailService;
    private readonly ILoggerService<UserController> _logger;
    private readonly IUserService _userService;

    public UserController(IEmailService emailService, ILoggerService<UserController> logger, IUserService userService)
    {
      _emailService = emailService;
      _logger = logger;
      _userService = userService;
    }


    /// <summary>
    /// Method is to get all users of project
    /// </summary>
    /// <response code="200">Returns users</response>

    // api/user/project-users
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpGet("project-users/{projectId:Guid}")]
    public async Task<ActionResult> GetUsersOfProject(Guid projectId)
    {
      var users = await _userService.GetUsersOfProjectAsync(projectId);

      return Ok(users);
    }


    /// <summary>
    /// Method is to get all users to add and remove them from project later
    /// </summary>
    /// <response code="200">Returns users</response>

    // api/user/users-with-project
    [Authorize(Roles = RoleType.MANAGER)]
    [HttpGet("users-with-project/{projectId:Guid}")]
    public async Task<ActionResult> GetUsersWithProjectStatus(Guid projectId)
    {
      var users = await _userService.GetUsersWithProjectStatusAsync(projectId);

      return Ok(users);
    }


    /// <summary>
    /// Method is to get all users that are in role of Employees or Manager
    /// </summary>
    /// <response code="200">Returns users(employees and managers)</response>

    // api/user/users-and-managers
    [Authorize(Roles = RoleType.ADMIN)]
    [HttpGet("users-and-managers")]
    public async Task<ActionResult> GetEmployeesAndManagers()
    {
      var adminId = Guid.Parse(User.FindFirstValue("UserId"));
      var users = await _userService.GetEmployeesAndManagersAsync(adminId);

      return Ok(users);
    }


    /// <summary>
    /// Method returns user that has an inputted Id property
    /// </summary>
    /// <response code="200">Returns user by Id property</response>

    //GET api/user/{id}
    [HttpGet("{id:Guid}", Name = "GetUserById")]
    public async Task<IActionResult> GetUserById(Guid id)
    {
      var user = await _userService.GetByIdAsync(id);

      return Ok(user);
    }


    /// <summary>
    /// Method changes user
    /// </summary>
    /// <response code="204">Returns nothing, user was successfully changed</response>

    //PUT api/users
    [HttpPut]
    [ProducesResponseType(204)]
    public async Task<IActionResult> UpdateUser(UserDto userDto)
    {
      _logger.LogInformation($"Start updating user - {userDto.Id}");

      await _userService.UpdateAsync(userDto);

      _logger.LogInformation($"User with Id {userDto.Id} was updated successfully");

      return NoContent();
    }


    /// <summary>
    /// Method is for setting role to users
    /// </summary>
    /// <response code="204">Returns nothing, successfully changed</response>

    // api/user/set-user-role
    [Authorize(Roles = RoleType.ADMIN)]
    [HttpPost("set-user-role")]
    [ProducesResponseType(204)]
    public async Task<ActionResult> SetUserRole(UserSetRoleDto userRoleDto)
    {
      _logger.LogInformation($"Start changing user role");

      await _userService.SetUserRoleAsync(userRoleDto);

      _logger.LogInformation($"User role was changed successfully");
      await _emailService.SendEmailAboutChangedRoleAsync(userRoleDto);

      return NoContent();
    }


    /// <summary>
    /// Method removes user
    /// </summary>
    /// <response code="204">Returns nothing, user was successfully removed</response>

    //DELETE api/user/{id}
    [Authorize(Roles = RoleType.ADMIN)]
    [HttpDelete("{id:Guid}")]
    [ProducesResponseType(204)]
    public async Task<IActionResult> RemoveUser(Guid id)
    {
      _logger.LogInformation($"Start removing user - {id}");

      await _userService.DeleteByIdAsync(id);

      _logger.LogInformation($"User with id {id} was removed successfully");

      return NoContent();
    }
  }
}
