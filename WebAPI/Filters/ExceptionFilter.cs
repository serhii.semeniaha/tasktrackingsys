﻿using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebAPI.Filters
{
  public class ExceptionFilter : ExceptionFilterAttribute
  {
    private readonly ILoggerService<ExceptionFilter> _logger;

    public ExceptionFilter(ILoggerService<ExceptionFilter> logger)
    {
      _logger = logger;
    }

    public override void OnException(ExceptionContext context)
    {
      switch (context.Exception.GetType().ToString())
      {
        case "DbQueryResultException":
          context.Result = new NotFoundObjectResult(context.Exception.Message);
          _logger.LogError(context.Exception.Message);
          context.ExceptionHandled = true;
          break;
        case "InvalidDataException":
          context.Result = new BadRequestObjectResult(context.Exception.Message);
          _logger.LogError(context.Exception.Message);
          context.ExceptionHandled = true;
          break;
        case "AuthException":
          context.Result = new NotFoundObjectResult(context.Exception.Message);
          _logger.LogError(context.Exception.Message);
          context.ExceptionHandled = true;
          break;
        default:
          context.Result = new BadRequestObjectResult(context.Exception.Message);
          _logger.LogError(context.Exception.Message);
          break;
      };
      base.OnException(context);
    }
  }
}
