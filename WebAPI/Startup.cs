using BLL.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebAPI.Extensions;
using WebAPI.Filters;
using WebAPI.Middlewares;

namespace WebAPI
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddWebApiDependecies();
      services.AddBllDependecies(Configuration);
      services.AddControllers(opt =>
        {
          opt.Filters.Add<ExceptionFilter>();
          opt.Filters.Add<ValidateModelsActionFilter>();
        })
        .ConfigureApiBehaviorOptions(opt => opt.SuppressModelStateInvalidFilter = true)
        .AddNewtonsoftJson();

      services.AddIdentityOptions();
      services.AddAuthenticationJwtConfig(Configuration);
      services.AddSwagger();
      services.AddCors();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      app.MigrateDb();

      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseMiddleware<ExceptionMiddleware>();
      }

      app.UseSwagger();

      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "TaskTrackingSysAPI");
        c.RoutePrefix = string.Empty;
      });

      app.UseHttpsRedirection();

      app.UseRouting();

      app.UseCors(builder => builder.WithOrigins(Configuration["JwtTokenSettings:Client_URL"]).AllowAnyMethod().AllowAnyHeader());

      app.UseAuthentication();
      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
